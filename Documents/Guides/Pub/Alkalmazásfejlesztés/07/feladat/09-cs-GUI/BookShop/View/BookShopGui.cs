using System;
using System.Collections.Generic;
using System.Windows.Forms;

using BookShop.Controller;
using BookShop.Model;
using BookShop.View.Dialogs;

namespace BookShop.View
{
    /// <summary>
    /// A BookShop alkalmaz�s f� ablaka.
    /// <para>
    /// A "partial" kulcssz� azt jelzi, hogy az oszt�ly k�dja t�bb f�jlban is szerepel.
    /// </para>
    /// </summary>
    public partial class BookShopGui : Form
    {
        #region mez�k
        private BookShopController m_controller;
        #endregion


        #region tulajdons�gok
        /// <summary>
        /// Visszaadja vagy be�ll�tja a <see cref="BookShopController"/> objektumot.
        /// </summary>
        public BookShopController Controller
        {
            get { return m_controller; }
            set { m_controller = value; }
        }
        #endregion


        #region konstruktor
        /// <summary>
        /// L�trehozza a <see cref="BookShopController"/> oszt�ly egy �j p�ld�ny�t.
        /// </summary>
        public BookShopGui()
        {
            InitializeComponent();
        }
        #endregion


        #region esem�nykezel�k
        /// <summary>
        /// A <see cref="newCustomerToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// l�trehozza �s megjelen�ti a <see cref="AddCustomerDialog"/> dial�gus
        /// ablakot �j vev� l�trehoz�s�ra.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void NewCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* �gyf�l hozz�ad�s�ra szolg�l� dial�gus ablak p�ld�nyos�t�sa �s megjelen�t�se.
             * "ShowDialog()" h�v�sakor tegy�k a dial�gusablakot "using" blokkba hogy
             * biztos�tsuk az er�forr�sok felszabad�t�s�t, mert ebben az eseben nem
             * h�v�dik meg automatikusan a "Dispose()" met�dus a dial�gusablak bez�r�sakor.
             * */
            using (AddCustomerDialog dialog = new AddCustomerDialog(this))
            {
                dialog.ShowDialog();
            }
        }


        /// <summary>
        /// A  <see cref="listCustomersToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// lekr�dezi a v�s�rl�k list�j�t, �s megjelen�ti a <see cref="customersDataGridView"/>
        /// t�bl�zatban.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<Customer> customers = m_controller.GetCustomerList();

            foreach (Customer customer in customers)
            {
                /* Bej�rjuk a lek�rdezett v�s�rl�k list�j�t, �s ki�rjuk az adataikat
                 * a conslora.
                 * FONTOS: ahhoz, hogy a consol is l�tsz�djon Windows Forms alkalmaz�s
                 * futtat�sakor, be kell �ll�tani a project "Properties" ablak�n az
                 * "Output type" leg�rd�l� men�b�l a "Console application" �rt�ket.
                 * */
                Console.WriteLine(customer);
            }

            /* A "DataGridView" vez�rl� nem k�pes kezelni az "IEnumerable" interface-t
             * megval�s�t� kollekci�kat, ez�rt megfelel� t�pus�ra, jelen esetben "IList"
             * t�pusra kell �talak�tani.
             * */

            /* Az "is" kulcssz� megvizsg�lja, hogy a bal oldalon l�v� t�pus a jobb
             * oldalinak lesz�rmazottja-e (interf�sz megval�s�t�s�t is lesz�rmaz�snak �rtelmezz�k)
             * �s ha nem, akkor l�trehoz egy "IList<T>" interface-t megval�s�t� kollekci�t.
             * */
            if (!(customers is IList<Customer>))
            {
                // A "List<T>" oszt�ly megval�s�tja az "IList<T>" interface-t.
                customers = new List<Customer>(customers);
            }

            /* NOTE:
             * null-ra �ll�tjuk, mert
             * a grid akkor "�rtelmezi" �jra a forr�s�t, ha az v�ltozik
             * a DAO visszaadja a t�rolt list�t, majd hozz�adunk egy elemet
             * ism�telt list�z�sn�l ugyanazon objektumot adja vissza a DAO
             * mivel nincs v�ltoz�s, ez�rt nem ker�l �jra�rtelmez�sre a forr�s
             * */
            customersDataGridView.DataSource = null;
            customersDataGridView.DataSource = customers;
            customersDataGridView.Visible = true;
        }


        /// <summary>
        /// Az <see cref="exitToolStripMenuItem"/> men�pont kiv�laszt�sakor kil�p�nk
        /// az alkalmaz�sb�l.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Az alkalmaz�s bez�r�sa.
            Application.Exit();
        }
        #endregion
    }
}

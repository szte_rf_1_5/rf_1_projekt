using System.Collections.Generic;

using BookShop.Dal;
using BookShop.Model;

namespace BookShop.Controller
{
    /// <summary>
    /// Ez az oszt�ly vez�rli az eg�sz programot, valamint a view �s model csomagokat
    /// k�ti �ssze. Itt tal�lhat� az �zleti logika (business logic) is.
    /// </summary>
    public class BookShopController
    {
        #region mez�k
        private IBookShopDao m_dao;
        #endregion


        #region tulajdons�gok
        /// <summary>
        /// Visszaadja vagy be�ll�tja a <see cref="BookShopDao"/> objektumot.
        /// </summary>
        public IBookShopDao BookShopDao
        {
            get { return m_dao; }
            set { m_dao = value; }
        }
        #endregion


        #region customer met�dusok
        /// <summary>
        /// Hozz�ad egy <see cref="Customer"/> objektumot az adatt�rhoz.
        /// </summary>
        /// <param name="customer">A t�roland� <see cref="Customer"/>.</param>
        /// <returns>Igaz, ha sikeresen t�rolva, egy�bk�nt hamis.</returns>
        public bool NewCustomer(Customer customer)
        {
            if (customer.Age < 14)
            {
                customer.Student = true;
            }
            else if (customer.Age > 62)
            {
                customer.Rented = true;
            }

            return m_dao.AddCustomer(customer);
        }


        /// <summary>
        /// Visszaadja a t�rolt <see cref="Customer"/> p�ld�nyokat.
        /// </summary>
        /// <returns>A t�rolt <see cref="Customer"/> p�ld�nyok kollekci�ja.</returns>
        public IEnumerable<Customer> GetCustomerList()
        {
            return m_dao.GetCustomers();
        }
        #endregion
    }
}

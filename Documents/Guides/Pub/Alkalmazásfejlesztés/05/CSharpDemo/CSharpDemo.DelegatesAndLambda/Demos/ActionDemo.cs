﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.DelegatesAndLambda.Demos
{
    #region demo code
    /// <summary>
    /// Action delegate demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: Action delegate
    /// Speciális delegate, olyan metódust reprezentál, aminek nincsen visszatérési értéke.
    /// Maximum 16 paramétere lehet.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/018hxwa8(v=vs.110).aspx
    /// </remarks>
    internal class ActionDemo : IDemo
    {
        private const string OUT_FORMAT_STRING = "Printed with the '{0}' method at {1}: {2}";

        //Egy Action meghatározott paraméter listával.
        private Action<string, DateTime> _twoParametersAction;


        public void Execute()
        {
            //A globális változó példányosítása egy előre definiált metódussal.
            _twoParametersAction = AnActionDelegateMethod;
            _twoParametersAction("Bla", DateTime.Now);

            //A globális változó példányosítása lambda (anonim) metódussal.
            _twoParametersAction = (text, time) => Console.WriteLine(OUT_FORMAT_STRING, "anonim", time, text);
            _twoParametersAction("Bla", DateTime.Now);
        }


        private void AnActionDelegateMethod(string text, DateTime time)
        {
            Console.WriteLine(OUT_FORMAT_STRING, "AnActionDelegateMethod", time, text);
        }
    }
    #endregion
}

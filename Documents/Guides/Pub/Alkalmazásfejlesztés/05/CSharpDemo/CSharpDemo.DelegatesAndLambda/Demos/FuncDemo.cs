﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.DelegatesAndLambda.Demos
{
    #region Demo code
    /// <summary>
    /// Func demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: Func deletate
    /// Speciális delegate, olyan metódust reprezentál, aminek van visszatérési értéke.
    /// Maximum 15 paramétere lehet.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/bb534960(v=vs.110).aspx
    /// </remarks>
    internal class FuncDemo : IDemo
    {
        private const string OUT_FORMAT_STRING = "Printed with the '{0}' method: {1}";

        //Egy globális Func meghatározott visszatérési értékkel és paraméter listával.
        private Func<double, double, int> _twoParameterFunction;


        public void Execute()
        {
            //A globális változó példányosítása egy előre definiált metódussal.
            _twoParameterFunction = AddAndRound;
            _twoParameterFunction(4.2, 4);


            //A globális változó példányosítása egy lambda metódussal.
            _twoParameterFunction = (a, b) =>
                {
                    int result = (int)Math.Round((a + b), 0);
                    Console.WriteLine(OUT_FORMAT_STRING, "anonim", result);
                    return result;
                };
            _twoParameterFunction(4.2, 4);
        }


        private int AddAndRound(double a, double b)
        {
            int result = (int)Math.Round((a + b), 0);
            Console.WriteLine(OUT_FORMAT_STRING, "AddAndRound", result);
            return result;
        }
    }
    #endregion
}

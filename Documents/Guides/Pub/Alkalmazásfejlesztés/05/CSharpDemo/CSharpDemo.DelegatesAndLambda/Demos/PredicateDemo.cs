﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.DelegatesAndLambda.Demos
{
    #region demo code
    /// <summary>
    /// Predicate demó.
    /// </summary>
    /// <remarks>
    /// Speciális delegate, olyan metódust reprezentál, aminek visszatérési értéke bool, és egy bemeneti paraméterrel rendelkezik.
    /// 
    ///  Források:
    ///     https://msdn.microsoft.com/en-us/library/bfcke1bz(v=vs.110).aspx
    /// </remarks>
    internal class PredicateDemo : IDemo
    {
        private const string OUT_FORMAT_STRING = "Printed with the '{0}' method: {1}";

        //Egy globális Predicate meghatározott paraméterrel.
        private Predicate<int> _predicate;


        public void Execute()
        {
            //A globális változó példányosítása egy előre definiált metódussal.
            _predicate = IsOdd;
            _predicate(4);

            //A globális változó példányosítása egy előre anonim metódussal.
            _predicate = (number) =>
                {
                    var isOdd = number % 2 == 1;
                    Console.WriteLine(OUT_FORMAT_STRING, "anonim", isOdd);
                    return isOdd;
                };
            _predicate(4);
        }


        private bool IsOdd(int number)
        {
            var isOdd = number % 2 == 1;
            Console.WriteLine(OUT_FORMAT_STRING, "IsOdd", isOdd);
            return isOdd;
        }
    }
    #endregion
}

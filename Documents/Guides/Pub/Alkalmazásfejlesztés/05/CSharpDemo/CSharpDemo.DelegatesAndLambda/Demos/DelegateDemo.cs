﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.DelegatesAndLambda.Demos
{
    #region demo code
    /// <summary>
    /// Delegate demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: delegate ("függvény pointer")
    /// A delegate típusok hivatkoznak egy metódusra meghatározott paraméter listával és visszatérési értékkel.
    /// Amikor példányosítunk egy delegate-t, meghatározhatjuk a megfelelő, hozzá társított metódust, ezután a társított 
    /// metódus meghívható a létrehozott delegate példányon keresztül.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/ms173171.aspx
    /// </remarks>
    internal class DelegateDemo : IDemo
    {
        private const string OUT_FORMAT_STRING = "Printed with the '{0}' method: {1}";

        //Egy globális delegate meghatározott visszatérési értékkel és paraméter listával.
        private delegate void OneParamDelegate(string text);


        public void Execute()
        {
            //A globális példányosítása egy előre definiált metódussal.
            OneParamDelegate handlerWithMethod = ADelegateMethod;
            handlerWithMethod("Bla");

            //A globális példányosítása egy lambda(anonim) metódussal.
            OneParamDelegate handlerWithLambdaMethod = (text) => Console.WriteLine(OUT_FORMAT_STRING, "anonim", text);
            handlerWithLambdaMethod("Bla");
        }


        private void ADelegateMethod(string text)
        {
            Console.WriteLine(OUT_FORMAT_STRING, "ADelegateMethod", text);
        }
    }
    #endregion
}

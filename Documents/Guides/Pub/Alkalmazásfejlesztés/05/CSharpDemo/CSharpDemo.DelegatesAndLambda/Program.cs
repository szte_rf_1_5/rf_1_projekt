﻿using ConsoleDemoUtils;
using CSharpDemo.DelegatesAndLambda.Demos;
using System;

namespace CSharpDemo.DelegatesAndLambda
{
    /// <summary>
    /// A futtatandó program.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<DelegateDemo>().Execute();
            new DemoManager<ActionDemo>().Execute();
            new DemoManager<PredicateDemo>().Execute();
            new DemoManager<FuncDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.ClassVsStruct.Demo
{
    #region demo code
    /// <summary>
    /// Calss vs Stuct demó.
    /// </summary>
    /// <remarks>
    /// Lényegi különbség C#-ban, hogy a stuktúra érték típusú, ameddig az osztály referencia típusú.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/ah19swz4.aspx
    ///     https://msdn.microsoft.com/en-us/library/x9afc042.aspx
    /// </remarks>
    internal class ClassVsStructDemo : IDemo
    {
        public void Execute()
        {
            MyClass myClass = new MyClass();
            myClass.Text = "My class text";
            Console.WriteLine("Created myClass with text: {0}", myClass.Text);

            MyStruct myStruct = new MyStruct();
            myStruct.Text = "My struct text";
            Console.WriteLine("Created myStruct with text: {0}", myStruct.Text);

            ModifyText(myClass);
            ModifyText(myStruct);
            Console.WriteLine("Modifing texts ...");

            Console.WriteLine("myClass text now: {0}", myClass.Text);
            Console.WriteLine("myStruct text now: {0}", myStruct.Text);
        }


        private void ModifyText(MyClass myclass)
        {
            myclass.Text = "Text modified";
        }


        private void ModifyText(MyStruct myStruct)
        {
            myStruct.Text = "Text modified";
        }


        #region MyClass
        public class MyClass
        {
            public string Text { get; set; }
        }
        #endregion


        #region MyStruct
        public struct MyStruct
        {
            public string Text { get; set; }
        }
        #endregion
    }
    #endregion
}

﻿using ConsoleDemoUtils;
using CSharpDemo.ClassVsStruct.Demo;

namespace CSharpDemo.ClassVsStruct
{
    /// <summary>
    /// A futtatandó program.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Az alkalmazás belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<ClassVsStructDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

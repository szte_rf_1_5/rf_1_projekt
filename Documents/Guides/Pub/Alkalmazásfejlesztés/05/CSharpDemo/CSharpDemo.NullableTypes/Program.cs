﻿using ConsoleDemoUtils;
using CSharpDemo.NullableTypes.Demos;

namespace CSharpDemo.NullableTypes
{
    /// <summary>
    /// A futtatandó osztály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<NullableTypesDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.NullableTypes.Demos
{
    #region demo code
    /// <summary>
    /// Nullable típusok demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: nullable típusok
    /// 
    /// Nullable típusok olyan érték típusokat reprezentálnak, amiknek lehet null étéket adni (A referencia típusok alapból kaphatnak null értéket).
    /// A T? jelölés a Nullable<T> rövid változata, ahol a T valamilyen érték típus.
    ///
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/1t3y8s4s.aspx
    ///     https://msdn.microsoft.com/en-us/library/ms173224.aspx
    /// </remarks>
    internal class NullableTypesDemo : IDemo
    {
        private const string SUCCESS_FORMAT_STRING = "Successfully converted '{0}' to int.";
        private const string FAIL_FORMAT_STRING = "Cannot convert '{0}' to int.";


        public void Execute()
        {
            const string intAsString = "12";

            int? parsedValue = ParseInt(intAsString);

            // Mind a két sor ugyanazt jelenti.
            int valueToDisplayOnUI = parsedValue ?? int.MaxValue;
            //int valueToDisplayOnUI = parsedValue.HasValue ? parsedValue.Value : int.MaxValue;

            if (parsedValue.HasValue)
            {
                Console.WriteLine(SUCCESS_FORMAT_STRING, intAsString);
            }
            else
            {
                Console.WriteLine(FAIL_FORMAT_STRING, intAsString);
            }
        }


        private int? ParseInt(string text)
        {
            int value;

            if (int.TryParse(text, out value))
            {
                return value;
            }

            return null;
        }
    }
    #endregion
}

﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleDemoUtils.Entities
{
    public class Student
    {
        public int ID { get; set; }

        public string First { get; set; }

        public string Last { get; set; }

        public uint Age { get; set; }

        public List<int> Scores { get; set; }

        public override string ToString()
        {
            return string.Concat(
                "Name: ", First, " ", Last,
                ", Score = ", Scores != null ? Scores.Sum() : 0);
        }
    }
}

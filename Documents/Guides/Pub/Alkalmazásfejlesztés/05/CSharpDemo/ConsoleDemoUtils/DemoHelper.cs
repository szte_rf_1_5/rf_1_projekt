﻿using System;

namespace ConsoleDemoUtils
{
	public static class DemoHelper
	{
		public static void DisplayNewLine()
		{
			Console.WriteLine();
		}

		public static void DisplayPressToTerminate()
		{
			Console.WriteLine("Press any key to terminate ...");
			Console.ReadKey();
		}

		internal static void DisplayBeginDemo(IDemo demo)
		{
			Console.WriteLine("{0} started:", demo.GetType().Name);
			Console.WriteLine();
		}

		internal static void DisplayEndDemo(IDemo demo)
		{
			Console.WriteLine();
			Console.WriteLine("----------------------------------------------------------");
			Console.WriteLine("\n");
		}
	}
}

﻿namespace ConsoleDemoUtils
{
	public class DemoManager<T> where T : class, IDemo, new()
	{
		public void Execute()
		{
			T demo = new T();

			DemoHelper.DisplayBeginDemo(demo);
			demo.Execute();
			DemoHelper.DisplayEndDemo(demo);
		}
	}
}

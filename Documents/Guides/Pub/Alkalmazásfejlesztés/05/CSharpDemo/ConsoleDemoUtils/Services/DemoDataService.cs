﻿using ConsoleDemoUtils.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleDemoUtils.Services
{
    public static class DemoDataService
    {
        public static IEnumerable<Student> GetDemoStudents()
        {
            List<Student> students = new List<Student>()
            {
                new Student {First="Claire", Last="O’Donnell"},
                new Student {First="Sven", Last="Mortensen"},
                new Student {First="Svetlana", Last="Omelchenko"},
                new Student {First="Sarah", Last="Kowal"},
                new Student {First="Ryan", Last="Klingaman"},
                new Student {First="Andrew", Last="Larew"},
                new Student {First="Kayla", Last="Lee"},
                new Student {First="Emma", Last="MacIntyre"},
                new Student {First="Brandon", Last=" Mandujano"},
                new Student {First="Jake", Last="Mazzeo"},
                new Student {First="Brittany", Last="Mortensen"},
                new Student {First="Kyle", Last="Omelchenko"},
            };

            Random rnd = new Random();
            var counter = 0;

            foreach (var student in students)
            {
                student.ID = counter++;
                student.Age = (uint)rnd.Next(14, 22);

                var scores = new List<int>();

                for (int i = 0; i < 5; i++)
                {
                    scores.Add(rnd.Next(20, 100));
                }

                student.Scores = scores;
            }

            return students;
        }
    }
}

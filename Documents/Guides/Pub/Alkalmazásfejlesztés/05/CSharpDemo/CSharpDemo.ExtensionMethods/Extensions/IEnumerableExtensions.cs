﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpDemo.ExtensionMethods.Extensions
{
    /// <summary>
    /// Bővítő metódusokat (extension methods) tartalmaz a <see cref="IEnumerable&lt;T&gt;"/> interfészhez.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Összefűzi az elemeket egyetlen string-be.
        /// </summary>
        /// <typeparam name="T">Az elemek típusa.</typeparam>
        /// <param name="items">A kollekció.</param>
        /// <param name="stringSelector">Az elem <see cref="string"/> reprezentációját biztosítja.</param>
        /// <param name="separator">Az elemeket elválasztó <see cref="string"/>.</param>
        /// <returns></returns>
        public static string ConcatenateItemsIntoString<T>(this IEnumerable<T> items, Func<T, string> stringSelector, string separator)
        {
            if (stringSelector == null)
                stringSelector = (item) => item.ToString();

            if (string.IsNullOrEmpty(separator))
                separator = ", ";

            StringBuilder stringBuilder = new StringBuilder();

            foreach (T item in items)
            {
                stringBuilder.AppendFormat("{0}{1}", stringSelector(item), separator);
            }

            return stringBuilder.ToString().TrimEnd(separator.ToCharArray());
        }
    }
}

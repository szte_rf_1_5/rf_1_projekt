﻿using ConsoleDemoUtils;
using ConsoleDemoUtils.Services;
//Névtér explicit megadása, a bővítő metódus használatára.
using CSharpDemo.ExtensionMethods.Extensions;
using System;

namespace CSharpDemo.ExtensionMethods.Demos
{
    #region demo code
    /// <summary>
    /// Bővítő metódusok demó.
    /// </summary>
    /// <remarks>
    /// A bővítő metódusok lehetővé teszik egy létező típus kiterjesztését a típus módosítása nélkül.
    /// Ezek különleges statikus metódusok, viszont ugyanúgy viselkednek, mintha egy objektum példány metódusai lennének.
    /// Az első paraméter határozza meg azt a típust, amin a metódus végrehajtható. Ez a paraméter a this kulcsszóval kezdődik.
    /// A bővítő metódusok csak abban az esetben használhatóak, ha a névterük explicit módon importálva van a using direktíva segítségével.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/bb383977.aspx
    /// </remarks>
    internal class ExtensionMethodDemo : IDemo
    {
        private const string HEADER = "Students concatenated into a string:";


        public void Execute()
        {
            var studentsAsString = DemoDataService.GetDemoStudents()
                //A bővítő metódus használata példány metódusként.
                .ConcatenateItemsIntoString(student => string.Concat(student.First, " ", student.Last), ", ");

            Console.WriteLine(HEADER);
            Console.WriteLine(studentsAsString);
        }
    }
    #endregion
}

﻿using ConsoleDemoUtils;
using CSharpDemo.ExtensionMethods.Demos;
using System;

namespace CSharpDemo.ExtensionMethods
{
    /// <summary>
    /// A futtatandó osztály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<ExtensionMethodDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

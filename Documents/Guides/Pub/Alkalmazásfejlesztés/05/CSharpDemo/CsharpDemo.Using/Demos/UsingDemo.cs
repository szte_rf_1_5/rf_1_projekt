﻿using ConsoleDemoUtils;
using System;
using System.IO;
using System.Reflection;

namespace CsharpDemo.Using.Demos
{
    #region demo code
    /// <summary>
    /// Using statement demó.
    /// </summary>
    /// <remarks>
    /// <para>Áttekintés: using utasítás</para>
    ///
    /// A using egy kényelmesen hasznáható szintaxist biztosít az IDisposable interfészt implementáló osztályok
    /// megfelelő használatára.
    /// Nem menedzselt erőforrások igénybevételénél fontos, hogy használat után az erőforrást felszabadítsuk. Ehhez az 
    /// IDisposable intefész által biztosított Dispose() metódust kell meghívnunk az adott menedzselt erőforrást birtokló osztályon.
    /// A using abban segít, hogy amint a vezérlés elhagyja a using (...) { ... } blokkját, meghívja a using-ban példányosított 
    /// osztály Dispose() metódusát.
    /// A using biztosítja, hogy az Dispose() metódus megfelelően legyen meghívva, még akkor is, ha kívétel dobódik.
    /// </remarks>
    internal class UsingDemo : IDemo
    {
        private const string RESOURCE_NAME = "CsharpDemo.Using.Assets.Texts.txt";
        private const string OUTPUT_STRING = "The content of the file:";


        /// <summary>
        /// Végrehajta a demót.
        /// </summary>
        /// <example>
        /// A következő using kódrészlet
        /// <code>
        /// using (Font font1 = new Font("Arial", 10.0f)) 
        /// {
        ///     byte charset = font1.GdiCharSet;
        /// }
        /// </code> futás időban az alábbi kódrészletté fordul:
        /// <code>
        /// {
        ///     Font font1 = new Font("Arial", 10.0f);
        ///     try
        ///     {
        ///         byte charset = font1.GdiCharSet;
        ///     }
        ///     finally
        ///     {
        ///         if (font1 != null)
        ///             ((IDisposable)font1).Dispose();
        ///     }
        /// }
        /// </code>
        /// </example>
        public void Execute()
        {
            var assembly = Assembly.GetExecutingAssembly();

            /*
             * Több using-ot is egymásba ágyazhatunk, illetve az átláthatóság kedvéért elegendő egyetlen blokkot nyitni
             * és a using utasításokat egymás alá felsorolni az alábbi módon.
             * A using utasításban példányosított változóknak nem lehet újra értéket adni és csak a using blokkjában hivatkozhatóak.
             */
            using (Stream stream = assembly.GetManifestResourceStream(RESOURCE_NAME))
            using (StreamReader reader = new StreamReader(stream))
            {
                string result = reader.ReadToEnd();

                Console.WriteLine(OUTPUT_STRING);
                Console.WriteLine(result);
            }
        }
    }
    #endregion
}

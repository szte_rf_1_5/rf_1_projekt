﻿using ConsoleDemoUtils;
using CsharpDemo.Using.Demos;

namespace CsharpDemo.Using
{
    /// <summary>
    /// A futtatandó osztály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<UsingDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

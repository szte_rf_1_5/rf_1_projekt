﻿using ConsoleDemoUtils;
using ConsoleDemoUtils.Entities;
using ConsoleDemoUtils.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace CSharpDemo.Parameters.Demos
{
    #region demo code
    /// <summary>
    /// Optional and named arguments demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: named arguments
    /// Lehetővé teszi, hogy egy adott argumentum társítását egy paraméterhez a paraméter neve alapján. Így nem kell fejből tudni
    /// vagy kikeresni a metódus paraméter listáját és a paraméterek sorrendjét.
    /// 
    /// Áttekintés: optional arguments
    /// Lehetővé teszi, hogy bizonyos paraméterekhez ne adjunk meg argumentumokat.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/dd264739.aspx
    /// </remarks>
    internal class OptionalParametersDemo : IDemo
    {
        private const string TOP_STUDENTS = "Top students:";
        private const string BEST_STUDENTS = "Best students";


        public void Execute()
        {
            const int top = 4;

            //Szimpla argumentum átadás, az opcionális argumentumok kihagyása.
            var topStudents = GetTopStudents(top);

            //Szimpla argumentum átadás, 1 opcionális argumentum megadása
            var bestStudents = GetTopStudents(top, true);

            //Named arguments használata (a paraméter sorrend felcserélhető).
            var topStudentsAbove18 = GetTopStudents(top, minAge: 18, orderByScore: false);

            Console.WriteLine(TOP_STUDENTS);

            foreach (var student in topStudents)
            {
                Console.WriteLine(student);
            }

            Console.WriteLine();
            Console.WriteLine(BEST_STUDENTS);

            foreach (var student in bestStudents)
            {
                Console.WriteLine(student);
            }
        }


        /// <summary>
        /// Visszaadja a legjobb vagy a listában a legelső a paraméterben megadott számú tanulót.
        /// </summary>
        /// <param name="studentsCount">A tanulók maximális száma.</param>
        /// <param name="orderByScore">Eldönti rendezni kell-e a tanulókat pontjaik alapján.</param>
        /// <param name="minAge">Egy tanuló minimális életkora; szűrő feltétel.</param>
        /// <returns>Visszadja a tanulok egy listáját.</returns>
        /// <remarks>
        /// Opcionális paraméterek: <see cref="orderByScore"/>, <see cref="minAge"/>
        /// Fontos, hogy az opcionális paraméterek a paraméter lista végén szerepelhetnek csak.
        /// Használható még az OptionalAttribute, ilyenkor az adott paraméter is opcionális lesz, viszont nem kell
        /// default értéket adni.
        /// </remarks>
        private List<Student> GetTopStudents(int studentsCount, [Optional] bool orderByScore, uint? minAge = null)
        {
            var students = DemoDataService.GetDemoStudents()
                .Where(student => minAge.HasValue ? student.Age > minAge : true)
                .Take(studentsCount).ToList();

            if (orderByScore)
            {
                students = students.OrderByDescending(x => x.Scores.Sum()).ToList();
            }

            return students;
        }
    }
    #endregion
}

﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.Parameters.Demos
{
    #region demo code
    /// <summary>
    /// out paraméter demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: out kulcsszó
    /// Az out kulcsó azt eredményezi, hogy az adott argumentum referenciájával fog átadódni (érték típus, a referencia típus alaból úgy adódik át).
    /// Ugyan olyan, mint a ref kulcsszó, azzal a kivétellel, hogy a ref kulcsszó használatához a változónak inicializálva kell lennie.
    /// Az out kulcsszónak szerepelnie kell a metódus definicióban és a hívó metódusban is.
    ///
    ///Források:
    ///     https://msdn.microsoft.com/en-us/library/t3c3bfhx.aspx
    /// </remarks>
    internal class OutParameterDemo : IDemo
    {
        private const string SUCCESS_FORMAT_STRING = "Successfully converted '{0}' to bool.";
        private const string FAIL_FORMAT_STRING = "Cannot convert '{0}' to bool.";


        public void Execute()
        {
            const string boolAsString = "1";

            bool value;

            //out kulcsszó a metódus hívásnál
            var success = TryParseBool(boolAsString, out value);

            if (success)
            {
                Console.WriteLine(SUCCESS_FORMAT_STRING, boolAsString);
            }
            else
            {
                Console.WriteLine(FAIL_FORMAT_STRING, boolAsString);
            }
        }


        //out kulcsszó a metódus deklarációjában
        private bool TryParseBool(string text, out bool boolValue)
        {
            boolValue = false;

            if (string.IsNullOrWhiteSpace(text))
                return false;

            var textLower = text.ToLower();

            if (textLower == "true" || textLower == "1")
            {
                boolValue = true;
                return true;
            }

            if (textLower == "false" || textLower == "0")
            {
                boolValue = false;
                return false;
            }

            return false;
        }
    }
    #endregion
}

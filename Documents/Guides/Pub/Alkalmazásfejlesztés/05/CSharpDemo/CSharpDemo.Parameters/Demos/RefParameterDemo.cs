﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.Parameters.Demos
{
    #region demo code
    /// <summary>
    /// ref paraméter demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: ref kulcsszó
    /// 
    /// Az ref kulcsszó azt eredményezi, hogy az adott argumentum referenciájával fog átadódni.
    /// Ennek eredménye, hogy bármilyen változatás a paraméteren a hívott metódusban, kihatással lesz a hívó metódusban is.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/14akc2c7.aspx
    /// </remarks>
    internal class RefParameterDemo : IDemo
    {
        private const string FORMAT_STIRING = "a: {0}, result: {1}";


        public void Execute()
        {
            int a = 0;
            var result = Increase(a);

            Console.WriteLine(FORMAT_STIRING, a, result);

            result = Increase(ref a);
            Console.WriteLine(FORMAT_STIRING, a, result);
        }


        //átadás étékkel.
        private int Increase(int number)
        {
            return number++;
        }


        //átadás referenciával (annak ellenére hogy ez egy érték típus).
        private int Increase(ref int number)
        {
            return number++;
        }
    }
    #endregion
}

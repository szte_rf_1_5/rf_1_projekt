﻿using ConsoleDemoUtils;
using CSharpDemo.Parameters.Demos;
using System;

namespace CSharpDemo.Parameters
{
    /// <summary>
    /// A futtatandó osztály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<OptionalParametersDemo>().Execute();
            new DemoManager<OutParameterDemo>().Execute();
            new DemoManager<RefParameterDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

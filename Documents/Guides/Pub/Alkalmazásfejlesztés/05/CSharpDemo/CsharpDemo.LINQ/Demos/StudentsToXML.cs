﻿using ConsoleDemoUtils;
using ConsoleDemoUtils.Services;
using System;
using System.Linq;
using System.Xml.Linq;
using CSharpDemo.ExtensionMethods.Extensions;

namespace CsharpDemo.LINQ.Demos
{
    #region demo code
    /// <summary>
    /// Students to XML demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: LINQ
    /// 
    /// A LINQ standard, könnyen megtanulható mintát nyújt adatok kezelésére. Visual Studio tartalmaz
    /// LINQ provider assembly-ket amik lehetővé teszik a LINQ használatát .NET-es kollekciókon
    /// (minden kollekción, ami implementálja az IEnumerable vagy IEnumerable<T> interfészt),
    /// SQL Server adatbázisokon, ADO.NET adatbázisokon, XML dokumentumokon, stb.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/bb397926.aspx
    ///     https://msdn.microsoft.com/en-us/library/system.xml.linq.xelement(v=vs.110).aspx
    /// </remarks>
    internal class StudentsToXML : IDemo
    {
        private const string HEADER_TEXT = "Generated XML from students:";


        public void Execute()
        {
            Console.WriteLine(HEADER_TEXT);

            var students = DemoDataService.GetDemoStudents();

            // Query létrehozása
            var studentsToXML = new XElement("Root",
                from student in students
                let x =  student.Scores.ConcatenateItemsIntoString(score => score.ToString(), ", ")
                select new XElement("student",
                    new XElement("First", student.First),
                    new XElement("Last", student.Last),
                    new XElement("Scores", x)) // "student" vége
                    ); // "Root" vége

            // Execute the query.
            Console.WriteLine(studentsToXML);
        }
    }
    #endregion
}

﻿using ConsoleDemoUtils;
using ConsoleDemoUtils.Services;
using System;
using System.Linq;

namespace CsharpDemo.LINQ.Demos
{
    #region demo code
    /// <summary>
    /// Studenst demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: LINQ
    /// 
    /// A LINQ standard, könnyen megtanulható mintát nyújt adatok kezelésére. Visual Studio tartalmaz
    /// LINQ provider assembly-ket amik lehetővé teszik a LINQ használatát .NET-es kollekciókon
    /// (minden kollekción, ami implementálja az IEnumerable vagy IEnumerable<T> interfészt),
    /// SQL Server adatbázisokon, ADO.NET adatbázisokon, XML dokumentumokon, stb.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/bb397926.aspx
    ///     https://msdn.microsoft.com/en-us/library/bb397696.aspx
    /// </remarks>
    internal class StudentsDemo : IDemo
    {
        private const string STUDENTS_ABOVE_AVG_FORMAT_STRING = "Students with score higher than {0}";
        private const string ANONIM_TYPE_STRING_FORMAT = "Student: {0}, score: {1}";


        public void Execute()
        {
            var students = DemoDataService.GetDemoStudents();
            int treshHold = (int)students.Average(student => student.Scores.Sum());

            Console.WriteLine(STUDENTS_ABOVE_AVG_FORMAT_STRING, treshHold);

            var query = students
                .Where(student => student.Scores.Sum() >= treshHold)
                                   //Anonim típus is létrehozható a select-ben
                .Select(student => new { Name = string.Concat(student.First, " ", student.Last), Score = student.Scores.Sum() });

            foreach (var item in query)
            {
                Console.WriteLine(ANONIM_TYPE_STRING_FORMAT, item.Name, item.Score);
            }
        }
    }
    #endregion
}

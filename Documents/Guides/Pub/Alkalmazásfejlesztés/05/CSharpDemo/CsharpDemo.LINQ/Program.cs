﻿using ConsoleDemoUtils;
using CsharpDemo.LINQ.Demos;
using System;

namespace CsharpDemo.LINQ
{
    /// <summary>
    /// A futtatandó oszály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<StudentsToXML>().Execute();
            new DemoManager<StudentsDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

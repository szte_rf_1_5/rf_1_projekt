﻿using ConsoleDemoUtils;
using CSharpDemo.Events.Demos;

namespace CSharpDemo.Events
{
    /// <summary>
    /// A futtatandó osztály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<EventsDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

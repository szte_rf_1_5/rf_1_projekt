﻿using ConsoleDemoUtils;
using System;

namespace CSharpDemo.Events.Demos
{
    #region demo code
    /// <summary>
    /// Események demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: események
    /// Az események lehetővé teszik egy osztálynak vagy objektumnak (küldő), hogy "értesítést" küldjön feliratkozott osztályoknak, objektumoknak (fogadó).
    /// Tulajdonságok:
    ///   - A feladó dönti el, hogy az esemény mikor lesz "kiküldve".
    ///   - Egy eseménynek több feliratkozója lehet és egy feliratkozó több eseményre feliratkozhat.
    ///   - Egy esemény feliratkozó nélkül sosem lesz kiküldve.
    ///   - Feliratkozás (+=), leiratkozás (-=) operátorokkal.
    /// </remarks>
    internal class EventsDemo : IDemo
    {
        private const string MESSAGE_RECEIVED_FORMAT_STRING = "Message received handled in an {0} method: \n '{1}'";

        public void Execute()
        {
            MailBox box = new MailBox();
            Sender sender = new Sender();

            //Feliratkozás lambda metódussal.
            box.MessageArrived += (s, e) =>
                {
                    Console.WriteLine(MESSAGE_RECEIVED_FORMAT_STRING, "anonim", e);
                };

            //Feliratkozás egy előre definiált metódussal.
            box.MessageArrived += box_MessageArrived;

            sender.SendMessage(box, "Demo message");

            //Leiratkozás az eseményről.
            box.MessageArrived -= box_MessageArrived;

            /* 
             * De mi van a labda metódussal?
             * Jelen pillantban ugyanis van még egy handler-ünk feliratkozva a eseményre, mégpedig a lambda metódus.
             * A fent látott esetben, nem tudjuk levenni az invocation list-ről az anonim metódust, így ezt azt jelenti, hogy ameddig
             * az esemény hívási listáján (invocation list) szerepel az anonim függvényünk, addig az objektum példányunkat a Garbage Collector nem fogja kitakarítani a memóriából.
             * (Ennek oka, hogy a háttérben egy delegate generálódik - mint ahogy valójában az EventHandler és EventHandler<T> is egy speciális delgate -,
             *  ami látható refernciaként jelenik meg az osztály delegate-jére az esemény hívási listájában).
             */

            sender.SendMessage(box, "Message after unsubscribe the method handler.");
        }


        #region helpers
        void box_MessageArrived(object sender, string e)
        {
            Console.WriteLine(MESSAGE_RECEIVED_FORMAT_STRING, "box_MessageArrived", e);
        }
        #endregion


        #region sender
        internal class Sender
        {
            internal void SendMessage(MailBox mailBox, string message)
            {
                mailBox.NewMessage = string.Concat(DateTime.Now.ToShortTimeString(), " - ", message);
            }
        }
        #endregion


        #region mail box
        internal class MailBox
        {
            /// <summary>
            /// Egy publikus, string argumentummal rendelkező esemény.
            /// </summary>
            public event EventHandler<string> MessageArrived;

            private string _newMessge;

            public string NewMessage
            {
                get { return _newMessge; }
                set
                {
                    if (_newMessge != value)
                    {
                        _newMessge = value;

                        //Vizsgáljuk meg, hogy van-e "felirtkozó"!
                        if (MessageArrived != null)
                        {
                            //sender, args
                            MessageArrived(this, _newMessge);
                        }
                    }
                }
            }
        }
        #endregion
    }
    #endregion
}

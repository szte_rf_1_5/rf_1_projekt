﻿using ConsoleDemoUtils;
using System;
using System.Collections.Generic;

namespace CSharpDemo.Properties.Demos
{
    #region demo code
    /// <summary>
    /// Property demó.
    /// </summary>
    /// <remarks>
    /// Áttekintés: property (tulajdonság)
    /// 
    /// A Property úgy teszi lehetővé kódfuttatást egy mező beállításakor és olvasásakor, hogy közben
    /// az adattag szintaxisát használja.
    /// Property-ben a get accessor a Property értékének a kinyerésére szolgál, míg a set accessor a property
    /// új értékét állítja be.
    /// Mind a get illetve set accessor-nak különbőző láthatóságokat lehet megadni:
    /// private, protected, protected internal, public.
    /// A value kulcsszó a set accessor-ban a Property-nek átadott új értéket tartalmazza.
    /// A property ami nem tartalmaz set accessor-t, read-only.
    /// 
    /// Források:
    ///     https://msdn.microsoft.com/en-us/library/x9fsa0sw.aspx
    ///     https://msdn.microsoft.com/en-us/library/6x16t2tx.aspx
    /// </remarks>
    internal class PropertiesDemo : IDemo
    {
        #region ExecuteDemo
        public void Execute()
        {
            PropertyHolder propertyHolder = new PropertyHolder();
            propertyHolder.AutoProperty = "Az auto property értékének beállítása osztályon kívülről.";
            propertyHolder.Property = "A property értékének beállítása osztályon kívülről.";
            //propertyHolder.ReadOnlyFromOutSideProperty = 5.0; Hiba, ez az osztályon kívülről read-only.
            //propertyHolder.ReadOnlyProperty = 5.0; Hiba, ez a property még az osztályon belül sem írható.


            /*Egy osztályt object initializer-rel is példányosíthatunk, ahol a property értékekek definiálhatjuk
              az alábbi módon.
             */
            PropertyHolder anotherPropertyHolder = new PropertyHolder
            {
                AutoProperty = "Az auto property értékének beállítása osztályon kívülről object initializerrel.",
                Property = "A property értékének beállítása osztályon kívülről object initializerrel.",
                //ReadOnlyFromOutSideProperty = 5.0, Hiba, ugyanúgy mint fentebb.
                //ReadOnlyProperty = 5.0, Hiba, ugyanúgy, mint fentebb.
            };

            Console.WriteLine("propertyHolder.AutoProperty: {0}", propertyHolder.AutoProperty);
            Console.WriteLine("propertyHolder.Property: {0}", propertyHolder.Property);
            Console.WriteLine("propertyHolder.ReadOnlyFromOutSideProperty: {0}", propertyHolder.ReadOnlyFromOutSideProperty);
            Console.WriteLine("propertyHolder.ReadOnlyProperty: {0}", propertyHolder.ReadOnlyProperty);

            //Indexed property elérése.
            var number = anotherPropertyHolder[2];
        }
        #endregion


        #region PropertyHolder
        /// <summary>
        /// Egy demó osztály, ami tartalmaz property példákat.
        /// </summary>
        public class PropertyHolder
        {
            #region Property
            private string _property; //backing store.

            /// <summary>
            /// Ez egy teljes elérésű property, az osztályon kívülről is állítható az értéke.
            /// </summary>
            /// <example>
            /// Java megfelelője a következő:
            /// <code>
            ///     private string property;
            /// 
            ///     public void setProperty(string value) {
            ///         property = value;
            ///     }
            /// 
            ///     public string getProperty() {
            ///         return property;
            ///     }
            /// </code>
            /// 
            /// <param>Használat</param>
            /// - Java:
            /// <code>
            ///     PropertyHolder.setProperty("Bla");
            ///     string variable = PropertyHolder.getProperty();
            /// </code>
            ///
            /// - C#:
            ///   <code>
            ///     PropertyHolder.Property = "Bla";
            ///     string variable = PropertyHolder.Property;
            ///   </code>
            /// </example>
            /// <remarks>
            /// A háttérben valóban getter és setter metódus párok fognak generálódni a propertynek megfelelően.
            /// </remarks>
            public string Property
            {
                get { return _property; }
                set { _property = value; }
            }
            #endregion


            /// <summary>
            /// Teljes elérésű Auto-Generated property.
            /// Ugyanaz a viselkedése, mint a fenti <see cref="Property"/>-nek, azzal a különbséggel,
            /// hogy a fordító fogja legenerálni a backing store-t.
            /// Backing store-nak szokás nevezni azt a (privát) membert akit a property kiszolgáltat.
            /// </summary>
            public string AutoProperty { get; set; }


            /// <summary>
            /// Az osztályon belül szabadon állítható a property értéke, viszont az osztályon kívülről
            /// read-only property-ként viselkedik.
            /// </summary>
            public double ReadOnlyFromOutSideProperty { get; private set; }


            private double _readonlyProperty = 5.0;

            /// <summary>
            /// Teljesen read-only property, még az osztályon belül sem állítható be az értéke, mivel
            /// nincs set accessor.
            /// </summary>
            public double ReadOnlyProperty
            {
                get { return _readonlyProperty; }
            }


            private readonly List<int> items = new List<int> { 0, 1, 2, 3, 4, 5, 6 };

            //Indexed property
            public int this[int index]
            {
                get
                {
                    if (index < 0 || index > items.Count)
                    {
                        throw new ArgumentOutOfRangeException("index");
                    }

                    return items[index];
                }
            }


            #region Constructor
            /// <summary>
            /// Létrehoz egy új példányt a <see cref="PropertyHolder"/> osztályból.
            /// </summary>
            public PropertyHolder()
            {
                ReadOnlyFromOutSideProperty = 4.0;
            }
            #endregion
        }
        #endregion
    }
    #endregion
}

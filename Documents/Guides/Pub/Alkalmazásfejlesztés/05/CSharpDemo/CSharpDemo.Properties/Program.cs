﻿using ConsoleDemoUtils;
using CSharpDemo.Properties.Demos;
using System;

namespace CSharpDemo.Properties
{
    /// <summary>
    /// A futtatandó osztály.
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program belépési pontja.
        /// </summary>
        /// <param name="args">Parancssori argumentumok.</param>
        static void Main(string[] args)
        {
            new DemoManager<PropertiesDemo>().Execute();

            DemoHelper.DisplayPressToTerminate();
        }
    }
}

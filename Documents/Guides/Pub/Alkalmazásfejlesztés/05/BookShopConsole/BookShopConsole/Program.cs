﻿using BookShopConsole.Controller;
using BookShopConsole.Model.Entities;
using BookShopConsole.View;
using System;

namespace BookShopConsole
{
    /// <summary>
    /// The program to be executed.
    /// </summary>
    class Program
    {
        /// <summary>
        /// The entry point of the program.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        static void Main(string[] args)
        {
            SeedEntitites();

            new BookShopConsoleView().Start();
        }


        #region seed storage
        private static void SeedEntitites()
        {
            BookShopController controller = new BookShopController();

            if (controller.GetBooks().Count == 0)
            {
                SeedBooks(controller);
            }
        }


        private static void SeedBooks(BookShopController controller)
        {
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                controller.AddBook(new Book
                {
                    Author = "Author " + i,
                    Title = "Title " + i,
                    Year = random.Next(1900, DateTime.Now.Year),
                });
            }
        }


        private static void SeedCustomers()
        {

        }
        #endregion
    }
}

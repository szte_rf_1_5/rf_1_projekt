﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShopConsole.Extensions
{
    /// <summary>
    /// Represents extension methods for <see cref="IEnumerable"/> and <see cref="IEnumerable&lt;T&gt"/>.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Concatenates items into on string.
        /// </summary>
        /// <typeparam name="T">The type of the items.</typeparam>
        /// <param name="items">The collection.</param>
        /// <param name="converter">The converter converts <see cref="T"/> to <see cref="string"/>.</param>
        /// <param name="separator">The separator, separates the items in the <see cref="string"/> representation.</param>
        /// <returns>The concatenated string.</returns>
        public static string ConcatenateIntoString<T>(this IEnumerable<T> items, Func<T, string> converter, string separator = " ")
        {
            StringBuilder builer = new StringBuilder();

            foreach (var item in items)
            {
                builer.AppendFormat("{0}{1}", converter(item), separator);
            }

            return builer.ToString().TrimEnd(separator.ToCharArray());
        }
    }
}

﻿using BookShopConsole.Model.Entities;
using System.Collections.Generic;

namespace BookShopConsole.Model.Repositories
{
    /// <summary>
    /// Defines data access to <see cref="Book"/> entities.
    /// </summary>
    public interface IBookRepository
    {
        /// <summary>
        /// Gets all <see cref="Book"/> entities.
        /// </summary>
        /// <returns>A list of <see cref="Book"/> entities.</returns>
        List<Book> GetBooks();

        /// <summary>
        /// Add a <see cref="Book"/> to the store.
        /// </summary>
        /// <param name="book">The <see cref="Book"/> to be stored.</param>
        void AddBook(Book book);

        /// <summary>
        /// Gets a <see cref="Book"/> by it's title.
        /// </summary>
        /// <param name="bookTitle">The title of the <see cref="Book"/>.</param>
        /// <returns>The <see cref="Book"/> with the given title.</returns>
        Book this[string bookTitle] { get; }
    }
}

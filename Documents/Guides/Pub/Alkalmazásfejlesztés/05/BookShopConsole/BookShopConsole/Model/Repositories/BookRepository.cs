﻿using BookShopConsole.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookShopConsole.Model.Repositories
{
    /// <summary>
    /// Represents the memory based implementation of the <see cref="IBookRepository"/> interface.
    /// </summary>
    public class BookRepository : IBookRepository
    {
        #region data members
        private List<Book> _books = new List<Book>();
        #endregion


        /// <summary>
        /// Gets all <see cref="Book"/> entities.
        /// </summary>
        /// <returns>A list of <see cref="Book"/> entities.</returns>
        public List<Book> GetBooks()
        {
            return _books;
        }


        /// <summary>
        /// Add a <see cref="Book"/> to the store.
        /// </summary>
        /// <param name="book">The <see cref="Book"/> to be stored.</param>
        public void AddBook(Book book)
        {
            _books.Add(book);
        }


        /// <summary>
        /// Gets a <see cref="Book"/> by it's name.
        /// </summary>
        /// <param name="bookTitle">The title of the <see cref="Book"/>.</param>
        /// <returns>The <see cref="Book"/> with the given name.</returns>
        public Book this[string bookTitle]
        {
            get
            {
                try
                {
                    return _books.First(book => book.Title == bookTitle);
                }
                catch (InvalidOperationException ex)
                {
                    throw new RepositoryException("Cannot get the book with title: " + bookTitle, ex);
                }
                catch (ArgumentNullException ex)
                {
                    throw new RepositoryException("Cannot get the book with title: " + bookTitle, ex);
                }
            }
        }
    }
}

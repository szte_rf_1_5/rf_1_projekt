﻿using BookShopConsole.Model.Entities;
using System.Collections.Generic;

namespace BookShopConsole.Model.Repositories
{
    /// <summary>
    /// Defines data access to <see cref="Customer"/> entities.
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets all <see cref="Customer"/> entities.
        /// </summary>
        /// <returns>A list of <see cref="Customer"/> entities.</returns>
        List<Customer> GetCustomers();


        /// <summary>
        /// Add a <see cref="Customer"/> to the store.
        /// </summary>
        /// <param name="book">The <see cref="Customer"/> to be stored.</param>
        bool AddCustomer(Customer customer);


        /// <summary>
        /// Gets a <see cref="Customer"/> by it's name.
        /// </summary>
        /// <param name="bookTitle">The name of the <see cref="Customer"/>.</param>
        /// <returns>The <see cref="Customer"/> with the given name.</returns>
        Customer this[string customerName] { get; }
    }
}

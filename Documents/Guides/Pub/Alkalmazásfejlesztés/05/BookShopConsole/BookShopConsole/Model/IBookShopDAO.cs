﻿using BookShopConsole.Model.Repositories;

namespace BookShopConsole.Model
{
    /// <summary>
    /// Defines the DAO.
    /// </summary>
    public interface IBookShopDAO
    {
        /// <summary>
        /// Gets <see cref="Book"/> repository.
        /// </summary>
        IBookRepository BookRepository { get; }

        /// <summary>
        /// Gets <see cref="Customer"/> repository.
        /// </summary>
        ICustomerRepository CustomerRepository { get; }
    }
}

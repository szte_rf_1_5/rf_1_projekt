﻿using BookShopConsole.Model.Entities;
using System;
using System.Collections.Generic;

namespace BookShopConsole.Model.Repositories
{
    /// <summary>
    /// Represents the memory based implementation of the <see cref="ICustomerRepository"/> interface.
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Gets all <see cref="Customer"/> entities.
        /// </summary>
        /// <returns>A list of <see cref="Customer"/> entities.</returns>
        public List<Customer> GetCustomers()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Add a <see cref="Customer"/> to the store.
        /// </summary>
        /// <param name="book">The <see cref="Customer"/> to be stored.</param>
        public bool AddCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Gets a <see cref="Customer"/> by it's name.
        /// </summary>
        /// <param name="bookTitle">The name of the <see cref="Customer"/>.</param>
        /// <returns>The <see cref="Customer"/> with the given name.</returns>
        public Customer this[string customerName]
        {
            get { throw new NotImplementedException(); }
        }
    }
}

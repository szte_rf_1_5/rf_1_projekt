﻿using BookShopConsole.Model.Repositories;

namespace BookShopConsole.Model
{
    /// <summary>
    /// Represents an implementation of the <see cref="IBookShopDAO"/> interface.
    /// </summary>
    public class BookShopDAO : IBookShopDAO
    {
        #region properties
        private readonly IBookRepository _bookShopRepository;

        /// <summary>
        /// Gets the <see cref="Book"/> repository.
        /// </summary>
        public IBookRepository BookRepository
        {
            get { return _bookShopRepository; }
        }


        private readonly ICustomerRepository _customerRepository;

        /// <summary>
        /// Gets the <see cref="Customer"/> repository.
        /// </summary>
        public ICustomerRepository CustomerRepository
        {
            get { return _customerRepository; }
        }
        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="BookShopDAO"/> class.
        /// </summary>
        public BookShopDAO()
        {
            _bookShopRepository = new BookRepository();
            _customerRepository = new CustomerRepository();
        }
    }
}

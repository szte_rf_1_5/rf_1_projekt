﻿using System;

namespace BookShopConsole.Model.Entities
{
    /// <summary>
    /// Represents a book.
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Gets or sets the author of the <see cref="Book"/>.
        /// </summary>
        public string Author { get; set; }


        /// <summary>
        /// Get or sets the title of the <see cref="Book"/>.
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// Gets or sets the production year of the <see cref="Book"/>.
        /// </summary>
        public int Year { get; set; }


        /// <summary>
        /// Gets the string representation of the <see cref="Book"/>.
        /// </summary>
        /// <returns>The string representation of the <see cref="Book"/>.</returns>
        public override string ToString()
        {
            return string.Format("Author: {0}, Title: {1}, Year: {2}", Author, Title, Year);
        }
    }
}

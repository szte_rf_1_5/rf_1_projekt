﻿namespace BookShopConsole.Model
{
    /// <summary>
    /// Gets a <see cref="IBookShopDAO"/> implementation provider unit.
    /// </summary>
    public static class BookShopDAOFactory
    {
        private static IBookShopDAO _bookShopDAO;

        /// <summary>
        /// Gets the <see cref="BookShopDAO"/> implementation of the <see cref="IBookShopDAO"/> interface
        /// in a singleton scope.
        /// </summary>
        /// <returns></returns>
        public static IBookShopDAO GetBookShopDAO()
        {
            return _bookShopDAO ?? (_bookShopDAO = new BookShopDAO());
        }
    }
}

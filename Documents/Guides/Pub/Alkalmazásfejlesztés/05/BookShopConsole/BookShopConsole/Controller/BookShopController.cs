﻿using BookShopConsole.Model;
using BookShopConsole.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace BookShopConsole.Controller
{
    /// <summary>
    /// Represents the bookshop controller.
    /// </summary>
    public class BookShopController
    {
        private readonly IBookShopDAO _bookShopDAO = BookShopDAOFactory.GetBookShopDAO();


        /// <summary>
        /// Gets all <see cref="Book"/> entities.
        /// </summary>
        /// <returns>The list of the <see cref="Book"/> entities.</returns>
        public List<Book> GetBooks()
        {
            return _bookShopDAO.BookRepository.GetBooks();
        }


        /// <summary>
        /// Adds a new <see cref="Book"/> entity.
        /// </summary>
        /// <param name="book">The <see cref="Book"/> entity.</param>
        public void AddBook(Book book)
        {
            var alreadyStoredThisBook = _bookShopDAO.BookRepository.GetBooks()
                .Any(storedBook => storedBook.Title == book.Title);

            if (!alreadyStoredThisBook)
            {
                _bookShopDAO.BookRepository.AddBook(book);
            }
            else
            {
                throw new ControllerException("The given book is already exists");
            }
        }


        /// <summary>
        /// Gets a <see cref="Book"/> with the given title.
        /// </summary>
        /// <param name="bookTitle">The title of the <see cref="Book"/>.</param>
        /// <returns>A <see cref="Book"/> with the given title.</returns>
        /// <exception cref="RepositoryException">If no book found or the argument is invalid.</exception>
        public Book GetBookByTitle(string bookTitle)
        {
            try
            {
                return _bookShopDAO.BookRepository[bookTitle];
            }
            catch (RepositoryException ex)
            {
                throw new ControllerException("Cannot get the book with the given title: {0}", ex);
            }
        }
    }
}

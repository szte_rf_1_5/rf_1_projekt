﻿using BookShopConsole.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookShopConsole.View
{
    /// <summary>
    /// Represents a class provides helper functions rendering data on the screen.
    /// </summary>
    public static class BookShopRenderer
    {
        /// <summary>
        /// Writes the given collection of <see cref="Book"/> entities using the <see cref="Console"/>.
        /// </summary>
        /// <param name="books">The collection of the <see cref="Book"/> entities.</param>
        public static void RenderBooks(IEnumerable<Book> books)
        {
            Console.WriteLine("All available book: {0}", books.Count());

            foreach (var book in books)
            {
                Console.WriteLine(book);
            }
        }


        /// <summary>
        /// Writes a <see cref="Book"/> entity using the <see cref="Console"/>.
        /// </summary>
        /// <param name="book"></param>
        public static void RenderBook(Book book)
        {
            Console.WriteLine(book);
        }


        /// <summary>
        /// Writes the given <see cref="Exception"/> using the <see cref="Console"/>.
        /// </summary>
        /// <param name="ex">The <see cref="Exception"/>.</param>
        public static void RenderException(Exception ex)
        {
            Console.WriteLine("The following exception occurred: \n {0}", ex);
        }
    }
}

﻿using BookShopConsole.Controller;
using BookShopConsole.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace BookShopConsole.View
{
    /// <summary>
    /// Represents the view in this application.
    /// </summary>
    public class BookShopConsoleView
    {
        #region constants
        private const string COMMAND_EXIT = "exit";
        private const string COMMAND_LIST_BOOKS = "lb";
        private const string COMMAND_GET_BOOK = "gb";
        private const string COMMAND_LIST_CUSTOMERS = "lc";
        private const string COMMAND_GET_CUSTOMER = "gc";
        #endregion


        #region data members
        private BookShopController _bookShopController = new BookShopController();

        private Dictionary<string, string> _uiActions = new Dictionary<string,string>();
        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="BookShopConsoleView"/> class.
        /// </summary>
        public BookShopConsoleView()
        {
            _uiActions.Add(COMMAND_LIST_BOOKS, "List Books.");
            _uiActions.Add(COMMAND_GET_BOOK, "Get book. Eg.: gb Title 1");
            _uiActions.Add(COMMAND_LIST_CUSTOMERS, "List Customers.");
            _uiActions.Add(COMMAND_GET_CUSTOMER, "Get customer. Eg.: gc Customer 1");
            _uiActions.Add(COMMAND_EXIT, "To close the application");
        }


        /// <summary>
        /// Starts the program.
        /// </summary>
        public void Start()
        {
            PrintCommands();

            while (true)
            {
                string UICommand = Console.ReadLine();

                if (HasExitEntered(UICommand))
                    break;

                ProcessCommand(UICommand);
            }
        }


        #region Helpers
        private void PrintCommands()
        {
            Console.WriteLine("You can use the following commands: ");

            foreach (var uiAction in _uiActions)
            {
                Console.WriteLine("{0} - {1}", uiAction.Key, uiAction.Value);
            }

            Console.WriteLine();
        }


        private bool HasExitEntered(string UICommand)
        {
            return UICommand.ToLower() == COMMAND_EXIT;
        }


        private void ProcessCommand(string UICommand)
        {
            var parameteredCommandReg = Regex.Match(UICommand, "[a-z][a-z] [a-zA-Z0-9 ]*", RegexOptions.CultureInvariant);

            var simpleCommandReg = Regex.Match(UICommand, "[a-z][a-z]", RegexOptions.CultureInvariant);

            string command = string.Empty;
            string parameter = string.Empty;

            if (parameteredCommandReg.Success)
            {
                var parts = parameteredCommandReg.Value.Split(' ');
                command = parts[0];
                parameter = UICommand.Substring(command.Length + 1);
            }
            else if (simpleCommandReg.Success)
            {
                command = simpleCommandReg.Value;
            }

            switch (command)
            {
                case COMMAND_LIST_BOOKS:
                    BookShopRenderer.RenderBooks(_bookShopController.GetBooks());
                    break;
                case COMMAND_GET_BOOK:
                    Book book;
                    if (TryGetBookByTitle(out book, parameter))
                    {
                        BookShopRenderer.RenderBook(book);
                    }
                    break;
                case COMMAND_LIST_CUSTOMERS:
                    Console.WriteLine("Not implemented");
                    break;
                case COMMAND_GET_CUSTOMER:
                    Console.WriteLine("Not implemented");
                    break;
                default:
                    Console.WriteLine("Unknown command '{0}'", UICommand);
                    PrintCommands();
                    break;
            }

            Console.WriteLine();
        }


        private bool TryGetBookByTitle(out Book book, string title)
        {
            try
            {
                book = _bookShopController.GetBookByTitle(title);
                return true;
            }
            catch (ControllerException ex)
            {
                BookShopRenderer.RenderException(ex);
                book = null;
                return false;
            }
        }
        #endregion
    }
}

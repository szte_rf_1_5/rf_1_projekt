﻿using System.Collections.Generic;
using BookShop.Model;

namespace BookShop.Dal
{
    public interface IBookShopDao
    {
        #region mutargy műveletek
        bool UjMutargy(Mutargy mutargy);

        IEnumerable<Mutargy> MutargyLista();
        #endregion

    }
}

using System;
using System.Collections.Generic;

using BookShop.Dal;
using BookShop.Model;

namespace BookShop.Controller
{
    /// <summary>
    /// Ez az oszt�ly vez�rli az eg�sz programot, valamint a view �s model csomagokat
    /// k�ti �ssze. Itt tal�lhat� az �zleti logika (business logic) is.
    /// </summary>
    public class BookShopController
    {
        #region mez�k
        private IBookShopDao m_dao;
        #endregion


        #region tulajdons�gok
        /// <summary>
        /// Visszaadja vagy be�ll�tja a <see cref="BookShopDao"/> objektumot.
        /// </summary>
        public IBookShopDao BookShopDao
        {
            get { return m_dao; }
            set { m_dao = value; }
        }
        #endregion


        #region mutargy met�dusok
        public bool UjMutargy(Mutargy mutargy)
        {
            return m_dao.UjMutargy(mutargy);
        }


        public IEnumerable<Mutargy> MutargyLista()
        {
            return m_dao.MutargyLista();
        }
        #endregion
    }
}

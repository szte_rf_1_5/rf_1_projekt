﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using BookShop.View;
using BookShop.Model;

namespace BookShop.View.Dialogs
{
    public partial class MutargyKivalasztas : Form
    {
        private BookShopGui gui;
        private List<Mutargy> mutargyak;
        public MutargyKivalasztas(BookShopGui gui)
        {
            InitializeComponent();

            this.gui = gui;

            mutargyak = new List<Mutargy>(gui.Controller.MutargyLista());
            foreach (Mutargy mutargy in mutargyak)
            {
                mutargyComboBox.Items.Add(mutargy.Nev);
            }

            mutargyComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            if (0 < mutargyak.Count)
            {
                mutargyComboBox.SelectedIndex = 0;
                categoryLabel.Text = mutargyak[0].Kategoria;
                yearLabel.Text = mutargyak[0].Ev.ToString();
            }
        }

        private void mutargyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            categoryLabel.Text = mutargyak[mutargyComboBox.SelectedIndex].Kategoria;
            yearLabel.Text = mutargyak[mutargyComboBox.SelectedIndex].Ev.ToString();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (0 < mutargyak.Count)
            {
                BookShopGui.ShowMessage("Kiválasztott műtárgy", "Név: " + mutargyak[mutargyComboBox.SelectedIndex].Nev + "\nKategória: " + mutargyak[mutargyComboBox.SelectedIndex].Kategoria + "\nÉv: " + mutargyak[mutargyComboBox.SelectedIndex].Ev);
            }
            else
            {
                BookShopGui.ShowMessage("Kiválasztott műtárgy", "Nincs műtárgy");
            }
            DialogResult = DialogResult.OK;
        }
    }
}

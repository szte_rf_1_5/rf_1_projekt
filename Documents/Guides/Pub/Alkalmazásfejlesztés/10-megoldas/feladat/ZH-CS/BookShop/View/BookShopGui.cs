using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

using BookShop.Controller;
using BookShop.Extensions;
using BookShop.Model;
using BookShop.View.Dialogs;

namespace BookShop.View
{
    /// <summary>
    /// A BookShop alkalmaz�s f� ablaka.
    /// <para>
    /// A "partial" kulcssz� azt jelzi, hogy az oszt�ly k�dja t�bb f�jlban is szerepel.
    /// </para>
    /// </summary>
    public partial class BookShopGui : Form
    {
        #region mez�k
        private BookShopController m_controller;
        #endregion


        #region tulajdons�gok
        /// <summary>
        /// Visszaadja vagy be�ll�tja a <see cref="BookShopController"/> objektumot.
        /// </summary>
        public BookShopController Controller
        {
            get { return m_controller; }
            set { m_controller = value; }
        }
        #endregion


        #region konstruktor
        /// <summary>
        /// L�trehozza a <see cref="BookShopController"/> oszt�ly egy �j p�ld�ny�t.
        /// </summary>
        public BookShopGui()
        {
            InitializeComponent();
        }
        #endregion


        #region esem�nykezel�k
        /// <summary>
        /// Az <see cref="exitToolStripMenuItem"/> men�pont kiv�laszt�sakor kil�p�nk
        /// az alkalmaz�sb�l.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Az alkalmaz�s bez�r�sa.
            Application.Exit();
        }
        #endregion

        #region seg�df�ggv�nyek
        public static void ShowMessage(String title, String message)
        {
            MessageBox.Show(
                message,
                title,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }
        #endregion

        private void mutargyHozzaadasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UjMutargy ujMutargy = new UjMutargy(this);
            ujMutargy.ShowDialog();
        }

        private void m�t�rgyakList�z�saToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IEnumerable<Mutargy> mutargyak = Controller.MutargyLista();
            mutargyDataGridView.DataSource = null;
            mutargyDataGridView.DataSource = mutargyak;
            mutargyDataGridView.Visible = true;
        }

        private void m�t�rgyKiv�laszt�saToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MutargyKivalasztas mutargyKivalasztas = new MutargyKivalasztas(this);
            mutargyKivalasztas.ShowDialog();
        }
    }
}

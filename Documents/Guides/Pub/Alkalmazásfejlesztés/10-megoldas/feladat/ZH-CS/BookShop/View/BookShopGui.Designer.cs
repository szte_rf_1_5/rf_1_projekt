namespace BookShop.View
{
    partial class BookShopGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookShopGui));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutargyHozzaadasaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m�t�rgyakList�z�saToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listPurchasesBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.mutargyDataGridView = new System.Windows.Forms.DataGridView();
            this.kiv�laszt�sToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m�t�rgyKiv�laszt�saToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mutargyDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataToolStripMenuItem,
            this.listToolStripMenuItem,
            this.toolStripMenuItem1,
            this.kiv�laszt�sToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(784, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mutargyHozzaadasaToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.dataToolStripMenuItem.Text = "&Felv�tel";
            // 
            // mutargyHozzaadasaToolStripMenuItem
            // 
            this.mutargyHozzaadasaToolStripMenuItem.Name = "mutargyHozzaadasaToolStripMenuItem";
            this.mutargyHozzaadasaToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.mutargyHozzaadasaToolStripMenuItem.Text = "M�t�rgy hozz�ad�sa";
            this.mutargyHozzaadasaToolStripMenuItem.Click += new System.EventHandler(this.mutargyHozzaadasaToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m�t�rgyakList�z�saToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.listToolStripMenuItem.Text = "&List�z�s";
            // 
            // m�t�rgyakList�z�saToolStripMenuItem
            // 
            this.m�t�rgyakList�z�saToolStripMenuItem.Name = "m�t�rgyakList�z�saToolStripMenuItem";
            this.m�t�rgyakList�z�saToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.m�t�rgyakList�z�saToolStripMenuItem.Text = "M�t�rgyak list�z�sa";
            this.m�t�rgyakList�z�saToolStripMenuItem.Click += new System.EventHandler(this.m�t�rgyakList�z�saToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // mutargyDataGridView
            // 
            this.mutargyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mutargyDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mutargyDataGridView.Location = new System.Drawing.Point(0, 24);
            this.mutargyDataGridView.Name = "mutargyDataGridView";
            this.mutargyDataGridView.Size = new System.Drawing.Size(784, 538);
            this.mutargyDataGridView.TabIndex = 1;
            // 
            // kiv�laszt�sToolStripMenuItem
            // 
            this.kiv�laszt�sToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m�t�rgyKiv�laszt�saToolStripMenuItem});
            this.kiv�laszt�sToolStripMenuItem.Name = "kiv�laszt�sToolStripMenuItem";
            this.kiv�laszt�sToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.kiv�laszt�sToolStripMenuItem.Text = "Kiv�laszt�s";
            // 
            // m�t�rgyKiv�laszt�saToolStripMenuItem
            // 
            this.m�t�rgyKiv�laszt�saToolStripMenuItem.Name = "m�t�rgyKiv�laszt�saToolStripMenuItem";
            this.m�t�rgyKiv�laszt�saToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.m�t�rgyKiv�laszt�saToolStripMenuItem.Text = "M�t�rgy kiv�laszt�sa";
            this.m�t�rgyKiv�laszt�saToolStripMenuItem.Click += new System.EventHandler(this.m�t�rgyKiv�laszt�saToolStripMenuItem_Click);
            // 
            // BookShopGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.mutargyDataGridView);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "BookShopGui";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Book Shop Application";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mutargyDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker listPurchasesBackgroundWorker;
        private System.Windows.Forms.ToolStripMenuItem mutargyHozzaadasaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m�t�rgyakList�z�saToolStripMenuItem;
        private System.Windows.Forms.DataGridView mutargyDataGridView;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem kiv�laszt�sToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m�t�rgyKiv�laszt�saToolStripMenuItem;
    }
}


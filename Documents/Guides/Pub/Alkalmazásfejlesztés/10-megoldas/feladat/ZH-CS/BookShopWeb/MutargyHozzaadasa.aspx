﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMasterPage.Master" AutoEventWireup="true" CodeBehind="MutargyHozzaadasa.aspx.cs" Inherits="BookShopWeb.MutargyHozzaadasa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 114px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="m_cphMaster" runat="server">
    <asp:Panel ID="Panel1" runat="server" GroupingText="Új műtárgy" Width="255px">
    <table style="width:100%;">
        <tr>
            <td class="auto-style1">Név:</td>
            <td>
                <asp:TextBox ID="nevTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Kategória:</td>
            <td>
                <asp:DropDownList ID="categoriaDropDownList" runat="server" Height="20px" Width="126px">
                    <asp:ListItem>Szobor</asp:ListItem>
                    <asp:ListItem>Festmény</asp:ListItem>
                    <asp:ListItem>Könyv</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Év:</td>
            <td>
                <asp:TextBox ID="evTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" class="auto-style1">
                <asp:Button ID="OKButton" runat="server" OnClick="OKButton_Click" Text="OK" />
            </td>
            <td align="center">
                <asp:Button ID="Button1" runat="server" PostBackUrl="~/Default.aspx" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:Content>

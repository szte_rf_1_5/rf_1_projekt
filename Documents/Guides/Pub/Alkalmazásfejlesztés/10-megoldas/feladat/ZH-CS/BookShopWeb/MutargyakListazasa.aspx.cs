﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookShopWeb
{
    public partial class MutargyakListazasa : Default
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mutargyakGridView.DataSource = Controller.MutargyLista();
            mutargyakGridView.DataBind();
        }
    }
}
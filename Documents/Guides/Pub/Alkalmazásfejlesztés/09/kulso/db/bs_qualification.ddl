create table qualifications(
  qid integer PRIMARY KEY AUTOINCREMENT,
  qualification text NOT NULL,
  UNIQUE (qualification)
);
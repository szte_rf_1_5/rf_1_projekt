﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using BookShop.Controller;
using BookShop.Extensions;
using BookShop.Model;
using BookShop.Resources;

namespace BookShop.View.Dialogs
{
    public partial class SellBookDialog : Form
    {
        #region mezők
        private BookShopGui gui;
        #endregion


        #region konstruktorok
        /// <summary>
        /// Létrehozza a <see cref="SellBookDialog"/> osztály egy új példányát.
        /// </summary>
        /// <param name="bookShopGui">A <see cref="BookShopGui"/> osztály egy példánya.</param>
        public SellBookDialog(BookShopGui gui)
        {
            this.gui = gui;

            InitializeComponent();
        }
        #endregion


        #region indítás
        private void SellBook_Load(object sender, EventArgs e)
        {
            BookShopController controller = gui.Controller;

            // Felhasználjuk a lentebb definiált extension method-ot
            customerChooserCombo.DataSource = controller.GetCustomerList().AsIList();
            bookChooserCombo.DataSource = controller.GetAvailableBookList().AsIList();
        }
        #endregion


        #region könyv eladás
        private void OkButton_Click(object sender, EventArgs e)
        {
            SellBookWorkItem workItem = new SellBookWorkItem
            {
                Book = bookChooserCombo.SelectedItem as Book,
                Customer = customerChooserCombo.SelectedItem as Customer
            };

            Thread sellBookThread = new Thread(SellBookThreadMethod);
            sellBookThread.Start(workItem);
        }


        /// <summary>
        /// Az osztály egy könyv eladás munkaegységet reprezentál.
        /// </summary>
        class SellBookWorkItem
        {
            #region tulajdonságok
            /// <summary>
            /// Beállítja vagy visszaadja a megvásárlolni kívánt könyvet.
            /// </summary>
            public Book Book { get; set; }


            /// <summary>
            /// Beállítja vagy visszaadja a vásárlót.
            /// </summary>
            public Customer Customer { get; set; }


            /// <summary>
            /// Beállítja vagy visszaadja, hogy az eladás sikeres-e.
            /// </summary>
            public bool IsSucceeded { get; set; }
            #endregion
        }


        private void SellBookThreadMethod(object parameter)
        {
            SellBookWorkItem workItem = parameter as SellBookWorkItem;

            Thread.Sleep(3000);

            // Eladáskor egyszerűen hivatkozhatunk a ComboBox-ok kiválasztott elemeire, 
            // mint Book és Customer entitásokra
            workItem.IsSucceeded = gui.Controller.SellBook(workItem.Book, workItem.Customer);

            SellBookThreadFinished(workItem);
        }


        /// <summary>
        /// Feldolgozza a <see cref="SellBookThreadMethod"/> háttérfolyamat eredményét.
        /// </summary>
        /// <param name="worktItem">A háttérfolyamat munkaegysége.</param>
        private void SellBookThreadFinished(SellBookWorkItem workItem)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<SellBookWorkItem>(SellBookThreadFinished), workItem);
                return;
            }

            if (!workItem.IsSucceeded)
            {
                MessageBox.Show(
                    this,
                    StringConstants.SellBookDialog_ErrorMessage_SellBook_Message,
                    StringConstants.SellBookDialog_ErrorMessage_SellBook_Title,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            DialogResult = DialogResult.OK;
        }
        #endregion
    }
}

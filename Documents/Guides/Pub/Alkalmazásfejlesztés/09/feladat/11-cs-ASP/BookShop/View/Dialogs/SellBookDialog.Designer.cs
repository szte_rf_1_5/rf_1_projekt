﻿namespace BookShop.View.Dialogs
{
    partial class SellBookDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customerChooserCombo = new System.Windows.Forms.ComboBox();
            this.bookChooserCombo = new System.Windows.Forms.ComboBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.customerChooserLabel = new System.Windows.Forms.Label();
            this.bookChooserLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // customerChooserCombo
            // 
            this.customerChooserCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customerChooserCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customerChooserCombo.FormattingEnabled = true;
            this.customerChooserCombo.Location = new System.Drawing.Point(72, 12);
            this.customerChooserCombo.Name = "customerChooserCombo";
            this.customerChooserCombo.Size = new System.Drawing.Size(392, 21);
            this.customerChooserCombo.TabIndex = 0;
            // 
            // bookChooserCombo
            // 
            this.bookChooserCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bookChooserCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bookChooserCombo.FormattingEnabled = true;
            this.bookChooserCombo.Location = new System.Drawing.Point(72, 39);
            this.bookChooserCombo.Name = "bookChooserCombo";
            this.bookChooserCombo.Size = new System.Drawing.Size(392, 21);
            this.bookChooserCombo.TabIndex = 1;
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.okButton.Location = new System.Drawing.Point(157, 66);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(238, 66);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // customerChooserLabel
            // 
            this.customerChooserLabel.AutoSize = true;
            this.customerChooserLabel.Location = new System.Drawing.Point(12, 15);
            this.customerChooserLabel.Name = "customerChooserLabel";
            this.customerChooserLabel.Size = new System.Drawing.Size(54, 13);
            this.customerChooserLabel.TabIndex = 4;
            this.customerChooserLabel.Text = "Customer:";
            // 
            // bookChooserLabel
            // 
            this.bookChooserLabel.AutoSize = true;
            this.bookChooserLabel.Location = new System.Drawing.Point(12, 42);
            this.bookChooserLabel.Name = "bookChooserLabel";
            this.bookChooserLabel.Size = new System.Drawing.Size(35, 13);
            this.bookChooserLabel.TabIndex = 5;
            this.bookChooserLabel.Text = "Book:";
            // 
            // SellBookDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(476, 98);
            this.Controls.Add(this.bookChooserLabel);
            this.Controls.Add(this.customerChooserLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.bookChooserCombo);
            this.Controls.Add(this.customerChooserCombo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SellBookDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sell Book...";
            this.Load += new System.EventHandler(this.SellBook_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox customerChooserCombo;
        private System.Windows.Forms.ComboBox bookChooserCombo;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label customerChooserLabel;
        private System.Windows.Forms.Label bookChooserLabel;
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

using BookShop.Controller;
using BookShop.Extensions;
using BookShop.Model;
using BookShop.View.Dialogs;

namespace BookShop.View
{
    /// <summary>
    /// A BookShop alkalmaz�s f� ablaka.
    /// <para>
    /// A "partial" kulcssz� azt jelzi, hogy az oszt�ly k�dja t�bb f�jlban is szerepel.
    /// </para>
    /// </summary>
    public partial class BookShopGui : Form
    {
        #region mez�k
        private BookShopController m_controller;
        #endregion


        #region tulajdons�gok
        /// <summary>
        /// Visszaadja vagy be�ll�tja a <see cref="BookShopController"/> objektumot.
        /// </summary>
        public BookShopController Controller
        {
            get { return m_controller; }
            set { m_controller = value; }
        }
        #endregion


        #region konstruktor
        /// <summary>
        /// L�trehozza a <see cref="BookShopController"/> oszt�ly egy �j p�ld�ny�t.
        /// </summary>
        public BookShopGui()
        {
            InitializeComponent();
        }
        #endregion


        #region esem�nykezel�k
        /// <summary>
        /// A <see cref="newCustomerToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// l�trehozza �s megjelen�ti a <see cref="AddCustomerDialog"/> dial�gus
        /// ablakot �j �gyf�l l�trehoz�s�ra.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void NewCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* �gyf�l hozz�ad�s�ra szolg�l� dial�gus ablak p�ld�nyos�t�sa �s megjelen�t�se.
             * "ShowDialog()" h�v�sakor tegy�k a dial�gusablakot "using" blokkba hogy
             * biztos�tsuk az er�forr�sok felszabad�t�s�t, mert ebben az eseben nem
             * h�v�dik meg automatikusan a "Dispose()" met�dus a dial�gusablak bez�r�sakor.
             * */
            using (AddCustomerDialog dialog = new AddCustomerDialog(this))
            {
                dialog.ShowDialog();
            }
        }


        /// <summary>
        /// A <see cref="buyBookToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// l�trehozza �s megjelen�ti a <see cref="BuyBookDialog"/> dial�gus
        /// ablakot k�nyv v�s�rl�s�ra.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void BuyBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (BuyBookDialog dialog = new BuyBookDialog(this))
            {
                dialog.ShowDialog();
            }
        }


        /// <summary>
        /// A <see cref="sellBookToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// l�trehozza �s megjelen�ti a <see cref="SellBookDialog"/> dial�gus
        /// ablakot k�nyv elad�s�ra.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void SellBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SellBookDialog dialog = new SellBookDialog(this))
            {
                dialog.ShowDialog();
            }
        }


        /// <summary>
        /// A  <see cref="listCustomersToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// lekr�dezi a v�s�rl�k list�j�t, �s megjelen�ti a <see cref="booksDataGridView"/>
        /// t�bl�zatban.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void ListCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread.Sleep(3000);

            IEnumerable<Customer> customers = m_controller.GetCustomerList();

            foreach (Customer customer in customers)
            {
                /* Bej�rjuk a lek�rdezett v�s�rl�k list�j�t, �s ki�rjuk az adataikat
                 * a console-ra.
                 * FONTOS: ahhoz, hogy a console is l�tsz�djon Windows Forms alkalmaz�s
                 * futtat�sakor, be kell �ll�tani a project "Properties" ablak�n az
                 * "Output type" leg�rd�l� men�b�l a "Console application" �rt�ket.
                 * */
                Console.WriteLine(customer);
            }

            /* A "DataGridView" vez�rl� nem k�pes kezelni az "IEnumerable" interface-t
             * megval�s�t� kollekci�kat, ez�rt megfelel� t�pus�ra, jelen esetben "IList"
             * t�pusra kell �talak�tani.
             * */

            ///* Az "is" kulcssz� megvizsg�lja, hogy a bal oldalon l�v� t�pus a jobb
            // * oldalinak lesz�rmazottja-e (interf�sz megval�s�t�s�t is lesz�rmaz�snak �rtelmezz�k)
            // * �s ha nem, akkor l�trehoz egy "IList<T>" interface-t megval�s�t� kollekci�t.
            // * */
            //if (!(customers is IList<Customer>))
            //{
            //    // A "List<T>" oszt�ly megval�s�tja az "IList<T>" interface-t.
            //    customers = new List<Customer>(customers);
            //}

            customers = customers.AsIList();

            /* NOTE:
             * null-ra �ll�tjuk, mert
             * a grid akkor "�rtelmezi" �jra a forr�s�t, ha az v�ltozik
             * a DAO visszaadja a t�rolt list�t, majd hozz�adunk egy elemet
             * ism�telt list�z�sn�l ugyanazon objektumot adja vissza a DAO
             * mivel nincs v�ltoz�s, ez�rt nem ker�l �jra�rtelmez�sre a forr�s
             * */
            customersDataGridView.DataSource = null;
            customersDataGridView.DataSource = customers;
            booksDataGridView.Visible = false;
            customersDataGridView.Visible = true;
            purchasesDataGridView.Visible = false;
        }


        /// <summary>
        /// A  <see cref="listCustomersToolStripMenuItem"/> men�pont kiv�laszt�sakor
        /// lekr�dezi a v�s�rl�k list�j�t, �s megjelen�ti a <see cref="booksDataGridView"/>
        /// t�bl�zatban.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void listAvailableBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread.Sleep(3000);

            IEnumerable<Book> books = m_controller.GetAvailableBookList();

            // ki�rjuk konzolra
            foreach (Book book in books)
            {
                Console.WriteLine(book);
            }

            //// konvert�ljuk a forr�st a grid-nek megfelel�re
            //if (!(books is IList<Book>))
            //{
            //    // A "List<T>" oszt�ly megval�s�tja az "IList<T>" interface-t.
            //    books = new List<Book>(books);
            //}

            books = books.AsIList();

            // megjelen�t�s
            booksDataGridView.DataSource = null;
            booksDataGridView.DataSource = books;
            booksDataGridView.Visible = true;
            customersDataGridView.Visible = false;
            purchasesDataGridView.Visible = false;
        }


        /// <summary>
        /// Az <see cref="listPurchasesStripMenuItem"/> men�pont kiv�laszt�sakor
        /// lekr�dezi a v�s�rl�sok list�j�t, �s megjelen�ti a <see cref="purchasesDataGridView"/>
        /// t�bl�zatban.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void ListPurchasesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listPurchasesBackgroundWorker.RunWorkerAsync();
        }


        /// <summary>
        /// Elv�gzi a k�nyv elad�sok list�z�sa m�veletet a h�tt�rben.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void listPurchasesBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(3000);

            IEnumerable<Purchase> purchases = Controller.GetPurchases();

            e.Result = purchases.AsIList();
        }


        /// <summary>
        /// Megjelen�ti a k�nyv elad�sok list�z�sa m�velete eredm�ny�t a UI-on.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void listPurchasesBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IList<Purchase> purchases = e.Result as IList<Purchase>;

            purchasesDataGridView.DataSource = null;
            purchasesDataGridView.DataSource = purchases;
            booksDataGridView.Visible = false;
            customersDataGridView.Visible = false;
            purchasesDataGridView.Visible = true;
        }


        /// <summary>
        /// Az <see cref="exitToolStripMenuItem"/> men�pont kiv�laszt�sakor kil�p�nk
        /// az alkalmaz�sb�l.
        /// </summary>
        /// <param name="sender">Az esem�ny k�ld�je.</param>
        /// <param name="e">Az esem�ny argumentumai.</param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Az alkalmaz�s bez�r�sa.
            Application.Exit();
        }
        #endregion
    }
}

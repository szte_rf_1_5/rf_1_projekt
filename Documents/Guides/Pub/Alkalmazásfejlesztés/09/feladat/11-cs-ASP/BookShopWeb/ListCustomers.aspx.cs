﻿using System;

namespace BookShopWeb
{
    /// <summary>
    /// Ez az oldal a vásárlókat listázza ki.
    /// </summary>
    public partial class ListCustomers : Default
    {
        #region eseménykezelők
        /// <summary>
        /// Betölti a táblázatba a vásárlók listáját.
        /// </summary>
        /// <param name="sender">Az esemény küldője.</param>
        /// <param name="e">Az esemény argumentumai.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            m_gvCustomers.DataSource = Controller.GetCustomerList();
            m_gvCustomers.DataBind();
        }
        #endregion
    }
}

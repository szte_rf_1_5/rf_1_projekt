﻿using BookShop.Model;

using System;

namespace BookShopWeb
{
    /// <summary>
    /// Ez az oldal a vásárlók felvételét valósítja meg.
    /// </summary>
    public partial class AddCustomer : Default
    {
        #region eseménykezelők
        /// <summary>
        /// Az oldal betöltésekor fut le.
        /// </summary>
        /// <param name="sender">Az esemény küldője.</param>
        /// <param name="e">Az esemény argumentumai.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Nincs használva.
        }


        /// <summary>
        /// A <see cref="m_btnOk"/> gomb kiválasztásakor menti az új vásárlót.
        /// </summary>
        /// <param name="sender">Az esemény küldője.</param>
        /// <param name="e">Az esemény argumentumai.</param>
        protected void BtnOk_Click(object sender, EventArgs e)
        {
            // Ha a név mező nincs kitöltve, tájékoztatjuk a felhasználót.
            if (String.IsNullOrEmpty(tb_name.Text))
            {
                ShowMessage("Customer name not specified.");
            }
            else
            {
                // Megpróbáljuk számmá alakítani az age mező tartalmát.
                int age;

                if (!int.TryParse(tb_age.Text, out age))
                {
                    ShowMessage("Age is not a valid number.");
                    return;
                }

                Customer customer = new Customer
                {
                    Name = tb_name.Text,
                    Age = age,
                    Female = m_rblGender.Items[0].Selected,
                    Grantee = m_cgGrantee.Checked,
                    Qualification = (string)m_ddlQualification.SelectedValue
                };

                bool isNewCustomerSaved = Controller.NewCustomer(customer);

                if(isNewCustomerSaved)
                {
                    // Sikeres mentés után visszatérünk a kezdőlapra.
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    ShowMessage("Save failed! Customer already exists.");
                }
            }
        }
        #endregion
    }
}

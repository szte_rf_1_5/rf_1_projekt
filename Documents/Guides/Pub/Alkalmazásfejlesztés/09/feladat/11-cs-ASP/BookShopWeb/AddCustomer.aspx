﻿<%@ Page Title="Add customer" Language="C#" AutoEventWireup="true" CodeBehind="AddCustomer.aspx.cs"
    Inherits="BookShopWeb.AddCustomer" MasterPageFile="~/MyMasterPage.Master" %>

<asp:Content ContentPlaceHolderID="m_cphMaster" runat="server">
    <%--Egy táblázatban hozzuk létre a Customerek felvételére alkalmas vezérlőkel.
    A tájékozódást segítő szövegek az egyszerűség kedvéért plain text-ek,
    de lehetnének asp Label-ek is. A komponensek kötelezően kaptak egy azonosítót
    mely segítségével ki tudjuk majd olvasni értéküket a c# kódból--%>
    <br />
    <br />
    <asp:Panel runat="server" Width="255px" GroupingText="Add customer">
    <table>
        <tr>
            <td>
                Name:
            </td>
            <td>
                <asp:TextBox ID="tb_name" runat="server" Width="136px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Age:
            </td>
            <td>
                <asp:TextBox ID="tb_age" runat="server" Width="136px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Gender:
            </td>
            <td>
                <asp:RadioButtonList ID="m_rblGender" runat="server">                    
                    <asp:ListItem Selected="True">Female</asp:ListItem>
                    <asp:ListItem>Male</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:CheckBox runat="server" ID="m_cgGrantee" Text="Grantee" />
            </td>
        </tr>
        <tr>
            <td>
                Qualification:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="m_ddlQualification">
                    <asp:ListItem>University</asp:ListItem>
                    <asp:ListItem>College</asp:ListItem>
                    <asp:ListItem>High School</asp:ListItem>
                    <asp:ListItem>Elementary School</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr align="center">
            <td>
                <asp:Button runat="server" ID="m_btnOk" Text="OK" OnClick="BtnOk_Click" />
            </td>
            <td>
                <asp:Button runat="server" ID="m_btnCancel" Text="Cancel" PostBackUrl="~/Default.aspx" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.Extensions
{
    /// <summary>
    /// Az extension method-oknak statikus osztályban kell lenniük.
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Amennyiben a <paramref name="collection"/> implementálja az
        /// <see cref="IList&lt;T&gt;"/> interfész, akkor visszaadja az objektumot,
        /// egyébként becsomagolja egy <see cref="IList&lt;T&gt;"/> interfészt
        /// implementáló objektumba.
        /// </summary>
        /// <typeparam name="T">A kollekció elemeinek típusa.</typeparam>
        /// <param name="collection">A kollekció.</param>
        /// <returns>A kollekció <see cref="IList&lt;T&gt;"/>-ként.</returns>
        public static IList<T> AsIList<T>(this IEnumerable<T> collection)
        {
            /* Az "as" utasítás megvizsgálja, hogy az objektum
             *   - az adott osztályból származik-e, vagy
             *   - az adott interfészt implementálja-e?
             * Ha igen, akkor egy megfelelő referenciát kapunk.
             * Ha nem, akkor a visszatérési érték null.
             * */

            IList<T> rvCollection = collection as IList<T>;

            return rvCollection == null
                ? new List<T>(collection)
                : rvCollection;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.Model
{
    public class Purchase
    {
        #region konstansok
        private const string TOSTRING_FORMAT_STRING =
            "{0} - SellDate: {2} - Customer: {3} - Book: {4}";
        #endregion


        #region tulajdonságok
        /// <summary>
        /// Beállítja vagy visszaadja a vásárló rekord id-ját.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Beállítja vagy visszaadja a vásárlás idejét.
        /// </summary>
        public DateTime SellDate { get; set; }

        /// <summary>
        /// Beállítja vagy visszaadja a vásárlót.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Beállítja vagy visszaadja a vásárolt könyvet.
        /// </summary>
        public Book Book { get; set; }
        #endregion


        #region Object felüldefiniálások
        /// <summary>
        /// Megadja az osztály szöveges reprezentációját.
        /// </summary>
        /// <returns>Az osztály szöveges reprezentációja.</returns>
        public override string ToString()
        {
            return string.Format(
                TOSTRING_FORMAT_STRING,
                Id, SellDate, Customer, Book);
        }
        #endregion
    }
}

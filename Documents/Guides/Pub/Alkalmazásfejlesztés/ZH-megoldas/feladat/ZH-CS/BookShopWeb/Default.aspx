﻿<%@ Page Language="C#" EnableSessionState="True" AutoEventWireup="true" Title="Home" CodeBehind="Default.aspx.cs"
    Inherits="BookShopWeb.Default" MasterPageFile="~/MyMasterPage.Master" %>

<asp:Content ContentPlaceHolderID="m_cphMaster" runat="server">
    <%--Az asp lapok részben a html szabvány elemeiből, részben pedig saját elemekből, direktívákból
        épülnek fel.
        Ez az oldal az alkalmazásunk kezdő lapja, a project indításakor ez az oldal jeleni meg.
        A többi laphoz hasonlóan ehhez az oldalhoz is definiálva van egy ún. MasterPage,
        ami az oldalak közös tulajdonságát definiálja.
        Ezáltal nem kell minden oldalon kialakítani az egyébként közös struktúrát, elegendő egy 
        Content típusú konténert elhelyezni az oldalon, hívatkozni a mester oldal "ContentPlaceHolder" elemére,
        és az adott oldara jellemző elemeket ezen konténerben elhelyezni.--%>
    <br />
    <%--A komponenseket a ToolBox-ból is ki lehet választani és rá lehet húzni a felületre Designer nézetben.
    Az "ID" property definiálja az elem egyedi azonosítóját, amivel hívatkozható.
    A "runat" property definiálja, hogy az adott komponens működése hol legyen kezelve.--%>
    <asp:Label runat="server" ID="m_lbHome" Text="Welcome to bookshop!"
        CssClass="labelStyle"></asp:Label>
</asp:Content>

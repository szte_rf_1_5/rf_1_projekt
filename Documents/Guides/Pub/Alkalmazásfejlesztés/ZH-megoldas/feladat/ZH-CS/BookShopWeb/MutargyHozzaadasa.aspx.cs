﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BookShop.Model;

namespace BookShopWeb
{
    public partial class MutargyHozzaadasa : Default
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OKButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nevTextBox.Text))
            {
                ShowMessage("Üres a Név mező");
            }
            else
            {
                int year;
                if (!int.TryParse(evTextBox.Text, out year))
                {
                    ShowMessage("Nem megfelelő az évszám!");
                    return;
                }
                Mutargy mutargy = new Mutargy()
                {
                    Nev = nevTextBox.Text,
                    Kategoria = (string)categoriaDropDownList.SelectedValue,
                    Ev = year
                };
                if (Controller.UjMutargy(mutargy))
                {
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    ShowMessage("Hiba az adatok mentése közben!");
                }
            }
        }
    }
}
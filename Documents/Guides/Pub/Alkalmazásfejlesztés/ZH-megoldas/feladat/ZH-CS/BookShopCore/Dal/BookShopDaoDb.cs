using System.Collections.Generic;
using System.Data.SQLite;
using BookShop.Model;
using System.Data;

namespace BookShop.Dal
{
    /// <summary>
    /// Ez az osztály az SQLite adatbázisból történő adatelérést szolgálja.
    /// </summary>
    public class BookShopDaoDb : IBookShopDao
    {
        #region konstansok
        private const string DATABASE_CONNECTION_STRING_FORMAT_STRING = "Data Source={0}";
        #endregion


        #region statikus tagok
        private readonly string s_connectionString;
        #endregion


        #region konstruktorok
        public BookShopDaoDb(string databasePath)
        {
            s_connectionString = string.Format(DATABASE_CONNECTION_STRING_FORMAT_STRING, databasePath);
        }
        #endregion


        #region mutargy műveletek
        public bool UjMutargy(Mutargy mutargy)
        {
            bool rvSucc = false;
            using (SQLiteConnection conn = new SQLiteConnection(s_connectionString))
            using (SQLiteCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = "INSERT INTO Mutargy (name, category, year) VALUES (@name, @category, @year)";
                command.Parameters.Add("name", DbType.String).Value = mutargy.Nev;
                command.Parameters.Add("category", DbType.String).Value = mutargy.Kategoria;
                command.Parameters.Add("year", DbType.Int32).Value = mutargy.Ev;
                rvSucc = command.ExecuteNonQuery() == 1;
            }
            return rvSucc;
        }

        public IEnumerable<Mutargy> MutargyLista()
        {
            List<Mutargy> mutargyak = new List<Mutargy>();
            using (SQLiteConnection conn = new SQLiteConnection(s_connectionString))
            using (SQLiteCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = "SELECT * FROM Mutargy";
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Mutargy mutargy = new Mutargy()
                        {
                            Id = reader.GetInt32(reader.GetOrdinal("id")),
                            Nev = reader.GetString(reader.GetOrdinal("name")),
                            Kategoria = reader.GetString(reader.GetOrdinal("category")),
                            Ev = reader.GetInt32(reader.GetOrdinal("year"))
                        };
                        mutargyak.Add(mutargy);
                    }
                }
            }
            return mutargyak;
        }
        #endregion
    }
}

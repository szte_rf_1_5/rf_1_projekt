﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using BookShop.Model;
using BookShop.Resources;

namespace BookShop.View.Dialogs
{
    public partial class UjMutargy : Form
    {
        private BookShopGui gui;
        public UjMutargy(BookShopGui gui)
        {
            InitializeComponent();
            this.gui = gui;

            kategoriaComboBox.Items.AddRange(new string[] { "Szobor", "Festmény", "Könyv" });
            kategoriaComboBox.SelectedIndex = 1;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nevTextBox.Text))
            {
                BookShopGui.ShowMessage(StringConstants.UjMutargy_UresNev, StringConstants.UjMutargy_UresNev_Uzenet);
            }
            else
            {
                Mutargy mutargy = new Mutargy()
                {
                    Nev = nevTextBox.Text,
                    Kategoria = (string)kategoriaComboBox.SelectedItem,
                    Ev = (int) evNumericUpDown.Value
                };
                if (gui.Controller.UjMutargy(mutargy))
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    BookShopGui.ShowMessage(StringConstants.UjMutargy_MentesiHiba, StringConstants.UjMutargy_MentesiHiba_Uzenet);
                }
            }

        }
    }
}

create table Mutargy
(
  id integer PRIMARY KEY AUTOINCREMENT,
  name text NOT NULL,
  category text NOT NULL,
  year integer NOT NULL,
  UNIQUE (name, year)
);

﻿using BookShop.Controller;
using BookShop.Dal;
using System;
using System.Web.UI;

namespace BookShopWeb
{
    /// <summary>
    /// Az alkalmazásunk alapértelmezett oldala.
    /// </summary>
    public partial class Default : Page
    {
        #region konstansok
        private const string SESSION_KEY_CONTROLLER = "Controller";

        // Mivel a web alkalmazás egy web szerver által fut, az alkalmazás gyökér könyvtára nem az asztali alkalmazásnál
        // megszokott "bin" könyvtár, ezért az ott használt relatív adatbázis elérési útvonal itt nem lesz érvényes.
        // Az elérési útvonal értékét át kell írni az adatbázis aktuális elérési útvonalára.
        private const string DATABASE_PATH = @"d:\teaching\alkfejl_2015\anyag\10\kulso\db\bookshop.db";
        #endregion


        #region tulajdonságok
        /// <summary>
        /// Visszaadja az egyetlen BookShopController példányt.
        /// </summary>
        protected BookShopController Controller
        {
            /* A BookShopController objektum tárolására a Session-t használjuk.
             * Ez az adott munkamenethez tartozó objektum.
             * Az oldalak betöltésekor minden esetben új oldalpéldányok jönnek létre, míg
             * a Session objektum az egész munkamenet alatt él.
             * */
            get 
            {
                // Ha még nem tároltuk el ezelőtt a controller egy példányát, akkor létrehozunk egyet, és
                // Eltároljuk a Session objektumban egy, az aktuális session ID-t is tartalmazó kulccsal.
                if (Session[SESSION_KEY_CONTROLLER] == null)
                {
                    BookShopController controller = new BookShopController
                    {
                        BookShopDao = new BookShopDaoDb(DATABASE_PATH)
                    };

                    Session[SESSION_KEY_CONTROLLER] = controller;
                }

                return (BookShopController)Session[SESSION_KEY_CONTROLLER];
            }
        }
        #endregion


        #region eseménykezelők
        /// <summary>
        /// Az oldal betöltésekor fut le.
        /// </summary>
        /// <param name="sender">Az esemény küldője.</param>
        /// <param name="e">Az esemény argumentumai.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Nincs használva.
        }
        #endregion


        #region üzenetek
        /// <summary>
        /// Megjelenít egy figyelmeztető üzenetet.
        /// </summary>
        /// <param name="message">Az üzenet tartalma.</param>
        protected void ShowMessage(string message)
        {
            Response.Write("<script type='text/javascript'>alert('" + message + "')</script>");
        }
        #endregion
    }
}

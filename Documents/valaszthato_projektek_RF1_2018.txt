
1. Online könyvesbolt (pl. bookline.hu)
2. Webshop (pl. tesco.hu)
3. Ingatlanközvetítő portál (pl. ingatlan.com)
4. Vasúti információszolgáltató és jegykiadó rendszer (pl. elvira.hu)
5. Online elektronikai áruház (pl. edigital.hu)
6. Repülőjegy foglaló (pl. wizzair.com)
7. Szállásfoglaló oldal (pl. booking.com)
8. Kuponos oldal (pl. bonuszbrigad.hu)
9. Telekocsi oldal (pl. oszkar.com)
10. Online ételrendelés (pl. netpincer.hu)
11. Utazási iroda (pl. vista.hu)
12. Tanulmányi rendszer (pl. www.etr.u-szeged.hu)
13. Online fogadás (pl. www.tippmix.hu)
14. Közösségi oldal (pl. facebook.com)
15. Szállodai nyilvántartó rendszer
16. Online bankfiók
17. Online könyvtár
18. Szállítmányozási cég
19. Önkormányzati ügyintéző portál
20. Humán erõforrás menedzsment (bérezés, szabadságok, beosztás, jutalom, teljesitmeny, felvétel-elbocsájtás stb.)
21. Autószervíz
22. Állatkert információs rendszer
23. Online menetrend (busz/villamos/troli menetrendek keresese, feltoltese, stb.)
24. Online filmkatalogus (filmek feltoltese, regisztralt userek velemyezhetik, szavazhatnak, kulonbozo szempont szerint lekerdezhetik, stb. ~ imdb)

Jelentkezes emailben, csapatnev + feladatsorszammal (max. 3, sorrend a prioritas), kiosztas erkezesi sorrendben!


<?php

include "menu.php";

login_check_admin();

if (isset($_POST["modify"])) {
    modify_user();
} else if(isset($_POST["activate"])){
    activate_user($_POST["activate"]);
} else if(isset($_POST["deactivate"])){
    deactivate_user($_POST["deactivate"]);
} else {
    users();
}

function users(){
    ?>
    <h2>Felhasználók (<?php echo get_users_count()?>)</h2>
    </br>
    </br>
    <?php
    
    pretable();

    $result=collect_data();

    detail_table($result);

}

function pretable(){
	?>
     <table style="width: 70%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 3%;text-align: left;">ID</td>
 					<td style="width: 10%;text-align: left;">Felhasználónév</td>
 					<td style="width: 10%;text-align: center;">Regisztrált</td>
 					<td style="width: 10%;text-align: center;">Módosítás</td>
                    <td style="width: 10%;text-align: center;">Név</td>
                    <td style="width: 7%;text-align: center;">Egyenleg</td>
                    <td style="width: 7%;text-align: center;">Város</td>
                    <td style="width: 7%;text-align: center;">Rossz jelszó</td>
                    <td style="width: 3%;text-align: center;">Aktív</td>
                    <td style="width: 6%;text-align: center;">Aktiválás/Deaktiválás</td>
 				</tr>
 	</thead>
 	</table>
 	<?php
}

function detail_table($result){
    while($row = mysqli_fetch_assoc($result)){
        ?>
        <table style="width: 70%">
            <tr>
                <td style="width: 3%;text-align: left;"><?php echo $row["ID"]?></td>
                <td style="width: 10%;text-align: left;"><?php echo $row["Felhasznalonev"]?></td>
                <td style="width: 10%;"><?php echo $row["Regisztracio"]?></td>
                <td style="width: 10%;"><?php echo $row["Mod_Timestamp"]?></td>
                <td style="width: 10%;"><?php echo $row["Vez_nev"]." ".$row["Ker_nev"]?></td>
                <td style="width: 7%;"><?php echo number_format($row["Egyenleg"],0,".",".")?> Ft</td>
                <td style="width: 7%;"><?php echo $row["Varos_ZIP"]?></td>
                <td style="width: 7%;"><?php echo $row["Sikertelen_login"]?></td>
                <?php
                if($row["aktiv"]==0){
                    ?>
                    <td style="width: 4%;"><img src="images/delete.png" style="width: 30px; height: 30px;" title="<?php echo $row["Felhasznalonev"]?>: Deaktivált"></td>
                    <td style="width: 13%;">
                        <form method="post" action="felhasznalo_kezeles.php">
                            <input src="images/complete.png" style="width: 30px; height: 30px;" type="image" title="<?php echo $row["Felhasznalonev"]?> aktiválása">
                            <input type="hidden" name="activate" value="<?php echo $row["Felhasznalonev"]?>">
                        </form>
                    </td>
                    <?php
                } else {
                    ?>
                    <td style="width: 4%;"><img src="images/complete.png" style="width: 30px; height: 30px;" title="<?php echo $row["Felhasznalonev"]?>: Aktivált"></td>
                    <td style="width: 13%;">
                        <form method="post" action="felhasznalo_kezeles.php">
                            <input src="images/delete.png" style="width: 30px; height: 30px;" type="image" title="<?php echo $row["Felhasznalonev"]?> deaktiválása">
                            <input type="hidden" name="deactivate" value="<?php echo $row["Felhasznalonev"]?>">
                        </form>
                    </td>
                    <?php
                }
                ?>
            </tr>
        </table>
       <?php
    }
}

function collect_data(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ("SELECT ID, Felhasznalonev, Regisztracio, Mod_Timestamp, Vez_nev, Ker_nev, 
                Egyenleg, Varos_ZIP, Sikertelen_login, aktiv FROM FELHASZNALO");
    $result = mysqli_query( $conn, $sql );

    mysqli_close($conn);
	return $result;
}

function get_users_count(){
    if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(1) AS num FROM felhasznalo");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);

    return $row["num"];
}

function deactivate_user($uname){
    if ( !($conn = connect() )) { 
        return false;
	}
	
	$stmt = mysqli_prepare( $conn, "UPDATE Felhasznalo SET Aktiv=0 WHERE Felhasznalonev='" . $uname . "'");
	
	if(mysqli_stmt_execute($stmt)){
		echo "<script>alert('Felhasználó sikeresen deaktiválva!')</script>";
		?><script language="JavaScript">
				document.location.href ="felhasznalo_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="felhasznalo_kezeles.php";
		</script><?php
	}
}

function activate_user($uname){
    if ( !($conn = connect() )) { 
        return false;
	}
	
	$stmt = mysqli_prepare( $conn, "UPDATE Felhasznalo SET Aktiv=1 WHERE Felhasznalonev='" . $uname . "'");
	
	if(mysqli_stmt_execute($stmt)){
		echo "<script>alert('Felhasználó sikeresen aktiválva!')</script>";
		?><script language="JavaScript">
				document.location.href ="felhasznalo_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="felhasznalo_kezeles.php";
		</script><?php
	}
}

?>
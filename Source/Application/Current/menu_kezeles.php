<?php

include "menu.php";

login_check_admin();

if (isset($_POST["delete_menu_item"])) {
    delete_menu_item();
} else{
	
	footer();
}

function pretable(){
	?>
     <table style="width: 60%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 12%;text-align: left;">Név</td>
 					<td style="width: 30%;text-align: left;">Leírás</td>
					<td style="width: 30%;text-align: left;">Törlés</td>
 				</tr>
 	</thead>
 	</table>
 	<?php
}

function detail_table($row){
	?>
	<table style="width: 60%">
		<tr>
			<td style="width: 14%;text-align: justify;"><?php echo $row["NEV"]?></td>
			<td style="width: 47%;text-align: left;"><?php echo $row["LEIRAS"]?></td>
			<td style="width: 5%;">
				<form method="post" action="menu_kezeles.php">
					<input src="images/delete.png" style="width: 30px; height: 30px;" type="image">
					<input type="hidden" name="delete_menu" value="<?php echo $row["ID"]?>">
				</form>
			</td>
		</tr>
	</table>
	<?php
}

function get_menu(){
	if ( !($conn = connect() )) { 
        	return false;
    }
	
	
	
     if(count_test("menü")){
	    $sql = ("SELECT ID, NEV, LEIRAS, LETREHOZ_DATUM, VALID_FROM, VALID_TO, MOD_TIMESTAMP FROM MENU WHERE");
	
	    $result = mysqli_query( $conn, $sql );

		?>
		<hr id="kisvonal" />
		   <p>Menük (<?php echo count_alkat("menü");?>)</p>  
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function delete_menu_item(){
	if ( !($conn = connect() )) { 
        return false;
    	}
	
	$stmt = mysqli_prepare( $conn, "DELETE FROM MENU WHERE ID='" . $_POST["delete_menu_item"] . "'");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres művelet!')</script>";
		?><script language="JavaScript">
				document.location.href ="menu_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="menu_kezeles.php";
		</script><?php
	}
}
?>
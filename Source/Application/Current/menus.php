<?php

include "menu.php";

if (isset($_POST["add_cart"])) {
    kosarba();
    get_termekek();
}else{
	get_termekek();
	footer();
}


function get_termekek(){
	?>
	<h2>Menük</h2>
	</br>
    <h3 id="title">Ételek</h3>
	    	<?php
	    		get_menu();
	    	?>
	    </br>
	<?php
}

function count_test($alkat){
	if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(ID) AS num FROM MENU");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    if ($row["num"] == 0) {
        mysqli_close($conn);
        return false;
    } else {
        mysqli_close($conn);
        return true;
    }
}

function count_alkat($alkat){
	if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(ID) AS num FROM MENU");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    mysqli_close($conn);
    
    return $row["num"];
}

function pretable(){
	?>
     <table style="width: 60%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 12%;text-align: left;">Név</td>
 					<td style="width: 30%;text-align: left;">Leírás</td>
 					<td style="width: 20%;text-align: left;">Mettől</td>
 					<td style="width: 5%;text-align: left;">Meddig</td>
 				</tr>
 	</thead>
 	</table>
 	<?php
}

function detail_table($row){
	?>
	<table style="width: 60%">
		<tr>
			<td style="width: 14%;text-align: justify;"><?php echo $row["NEV"]?></td>
			<td style="width: 47%;text-align: left;"><?php echo $row["LEIRAS"]?></td>
			<td style="text-align: justify;width: 30%;"><?php echo $row["VALID_FROM"]?></td>
			<td style="text-align: justify;width: 30%;"><?php echo $row["VALID_TO"]?></td>
		</tr>
	</table>
	<?php
}

function get_menu(){
	if ( !($conn = connect() )) { 
        	return false;
    }
	
	
	
     if(count_test("menü")){
	    $sql = ("SELECT ID, NEV, LEIRAS, LETREHOZ_DATUM, VALID_FROM, VALID_TO, MOD_TIMESTAMP FROM MENU WHERE VALID_TO > SYSDATE()");
		
		// $date = date('Y-m-d');
		// $date between 'VALID_FROM' and 'VALID_TO'
	    $result = mysqli_query( $conn, $sql );

		?>
		<hr id="kisvonal" />
		   <p>Menük (<?php echo count_alkat("menü");?>)</p>  
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}
?>
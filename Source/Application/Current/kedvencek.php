<?php

include "menu.php";

pretable();
detail_table(lekerdez());

function pretable(){
	?>
	<h2> Kedvenc termékek</h2>
     <table style="width: 40%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 25%;text-align: left;">Sorszám</td>
 					<td style="width: 25%;text-align: left;">Kép</td>
 					<td style="width: 25%;text-align: left;">Termék</td>
 					<td style="width: 15%;text-align: left;">Rendelt darabszám</td>
 				</tr>
 	</thead>
 	</table>
<?php
}

function detail_table($result){
	?>
	<table style="width: 40%">
		<?php
			$rank = 1;
				while ($row = mysqli_fetch_assoc($result)) {
					
				?>
				<tr>
				<td style="width: 25%;text-align: justify;"><?php echo $rank?></td>
				<td style="width: 30%;text-align: left;">
				<?php
					if(!is_null($row["SRC"])){
					?>
					<img src="images/<?php echo $row["SRC"]?>" style="float:left;width: 100px; height: 100px;">
					<?php
					}else{
						?>
						<img src="images/soon.png" style="float:left;width: 100px; height: 100px;">
					<?php
					}
					?>
				</td>
				<td style="width: 35%;text-align: justify;"><?php echo $row["NEV"]?></td>
				<td style="width: 25%;text-align: justify;"><?php echo $row["TOP"]?></td>
				</tr>
				<?php
				$rank++;
			}
			?>
	</table>
	<?php
}


function lekerdez(){
	if ( !($conn = connect() )) {
        return false;
    }
	$sql = "SELECT SRC,NEV,SUM(rendelesegyseg.Mennyiseg) AS TOP
			FROM termek,rendelesegyseg
			WHERE termek.ID = rendelesegyseg.Termek_ID
			GROUP BY termek.nev
			ORDER BY TOP DESC
			LIMIT 5";
	$result = mysqli_query($conn,$sql);
	
	mysqli_close($conn);
	
	return $result;
}
?>
<?php

function connect() {
	$conn = mysqli_connect("localhost", "root", "") or die("Csatlakozási hiba");
	if ( false == mysqli_select_db($conn, "emeal" )  ) {
		echo "Connection failed";
		return null;
	}
	 
	mysqli_query($conn, 'SET character_set_results=utf8');
	mysqli_set_charset($conn, 'utf8');
	
	return $conn;
}

function footer(){
	?>
	</br>
	</br>
	</br>
	<footer>&copy; Copyright The5Crew#2018</footer>
	</br>
	<?php
}

function getBalance($username){
	if ( !($conn = connect() )) { 
		return false;
	}

	$sql = ("SELECT EGYENLEG AS balance FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $username . "'");

	$result = mysqli_query( $conn, $sql );
	$row = mysqli_fetch_assoc($result);

	return $row["balance"];
}

function getCartNumber($array){
	$sum=0;
	foreach($array as $i => $i_value) {
        $sum+=$i_value;
	}
	return $sum;
}

function kosarba(){
	if(isset($_SESSION["user"])){
		if(isset($_SESSION["cart"][$_POST["add_cart"]])){
			$_SESSION["cart"][$_POST["add_cart"]]++;
		}else{
			$_SESSION["cart"] += [$_POST["add_cart"]=>1];
		}
		?><script language="JavaScript">
				history.back(true);
		</script><?php
	}else{
		echo "<script>alert('Jelentkezz be, ha használni szeretnéd a kosaradat!')</script>";
		?><script language="JavaScript">
				document.location.href ="login.php";
		</script><?php
	}
}

function kosarbol(){
	if(isset($_SESSION["user"])){
		if(isset($_SESSION["cart"][$_POST["minus_cart"]])){
			if($_SESSION["cart"][$_POST["minus_cart"]]>1){
				$_SESSION["cart"][$_POST["minus_cart"]]--;
			} else{
				unset($_SESSION["cart"][$_POST["minus_cart"]]);
			}
		}
		?><script language="JavaScript">
				history.back(true);
		</script><?php
	}else{
		echo "<script>alert('Jelentkezz be, ha használni szeretnéd a kosaradat!')</script>";
		?><script language="JavaScript">
				document.location.href ="login.php";
		</script><?php
	}
}

function kosar_product_clear(){
	if(isset($_SESSION["user"])){
		unset($_SESSION["cart"][$_POST["clear_product"]]);
	}
	?><script language="JavaScript">
				history.back(true);
	</script><?php
}

function kosar_clear(){
	if(isset($_SESSION["user"])){
		unset($_SESSION["cart"]);
	}
}

function get_news_index(){
	if ( !($conn = connect() )) { 
        	return false;
    }

    $sql = ("SELECT NEV, LEIRAS, LETREHOZ_DATUM, (select FELHASZNALONEV from FELHASZNALO WHERE FELHASZNALO.ID=HIR.FELH_ID) AS FELHASZNALO FROM hir ORDER BY LETREHOZ_DATUM DESC");
     $result = mysqli_query( $conn, $sql );

     while($row = mysqli_fetch_assoc($result)){
     	?>
     	</br>
 		<table id='hir'>
 			<tr id='none' style="text-align: left;">
 				<th><?php echo $row["NEV"]?></th>
 			</tr>
 			<tr id='none' style="text-align: justify;"">
 				<td><?php echo $row["LEIRAS"]?></td>
 			</tr>
 			<tr id='none' style="font-weight: bold; font-size: 10px; font-style: oblique; text-align: right;">
 				<td><?php echo $row["FELHASZNALO"].", ".$row["LETREHOZ_DATUM"]?></td>
 			</tr>
 		</table>
		</br>
		<?php
     }
}
	
function admin_e($username) {
	if ( !($conn = connect() )) { 
        	return false;
    }

    $sql = ("SELECT ADMIN AS num FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $username . "'");

    $result = mysqli_query( $conn, $sql );
	$row = mysqli_fetch_assoc($result);

    if ($row["num"] == 1) {
        mysqli_close($conn);
        return true;
    } else {
        mysqli_close($conn);
        return false;
    }
}

function login_check_admin_develop(){
	if (!isset($_SESSION["user"])) {
		echo "<script>alert('Jelentkezz be, ha használni szeretnéd ezt az oldalt!')</script>";
		?><script language="JavaScript">
				document.location.href ="index.php";
		</script><?php
	} else if (!(admin_e($_SESSION["user"]))) {
		echo "<script>alert('Nincs jogosultságod az oldal megtekintéséhez!')</script>";
		?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php
	} else {
		echo "<script>alert('Ez a funkció fejlesztésre vár!')</script>";
		?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php
	}
}

function login_check_develop(){
	if (!isset($_SESSION["user"])) {
		echo "<script>alert('Jelentkezz be, ha használni szeretnéd ezt az oldalt!')</script>";
		?><script language="JavaScript">
				document.location.href ="index.php";
		</script><?php
	} else {
		echo "<script>alert('Ez a funkció fejlesztésre vár!')</script>";
		?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php	
	} 
}

function develop(){
	echo "<script>alert('Ez a funkció fejlesztésre vár!')</script>";
		?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php
} 

function login_check(){
	if (!isset($_SESSION["user"])) {
		echo "<script>alert('Jelentkezz be, ha használni szeretnéd ezt az oldalt!')</script>";
		?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php
	}
}

function login_check_admin(){
	if (!isset($_SESSION["user"])) {
		echo "<script>alert('Jelentkezz be, ha használni szeretnéd ezt az oldalt!')</script>";
		?><script language="JavaScript">
				document.location.href ="index.php";
		</script><?php
	} else if(!(admin_e($_SESSION["user"]))) {
		echo "<script>alert('Nincs jogosultságod az oldal megtekintéséhez!')</script>";
		?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php
	}
}

?>
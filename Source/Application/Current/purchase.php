<?php

include "menu.php";

login_check();

if (isset($_POST["purchase"])){
    vasarlas($_POST["purchase"]);
    footer();
}else if (isset($_POST["purchase_confirm"])){
    vasarlas_confirm();
    footer();
}else {
    echo "<h3>Köszönjük, hamarosan átveheted a rendelésed!</h3>";
    footer();
}


function vasarlas($sum){
    ?>
    <h2>Rendelés véglegesítése</h2>
   	</br>
    <form method="post" action="purchase.php">
        <table>
            <tr>
                <td>Végső fizetendő összeg</td>
                <td><b><?php echo number_format($sum,0,".",".")?> Ft</b></td>
            </tr>
            <tr>
                <td>Rendelkezésre álló egyenleg</td>
                <td><b><?php echo number_format(getBalance($_SESSION["user"]),0,".",".")?> Ft</b></td>
            </tr>
            <tr>
                <td>Fizetési mód</td>
                <td>
                    <select name="fiz_mod">
                        <option selected value="Helyszíni fizetés">Helyszíni fizetés</option>
                        <option value="Online Egyenleg">Online Egyenleg</option>
                    </select>
                </td>

            <tr>
        </table>
        </br></br>
        <input type="hidden" name="purchase_confirm" value="true">
        <input type="hidden" name="purchase_amount" value="<?php echo $sum?>">
        <input type="submit" name="done" value="Rendelés leadása">
    </form>
    <?php
}

function vasarlas_confirm(){
    if($_POST["fiz_mod"] == "Online Egyenleg"){
        //fgv.php -> getBalance(...);
        $balance=getBalance($_SESSION["user"]);
        if(($balance-$_POST["purchase_amount"])<0){
            echo "<script>alert('Nincs elég egyenleged a vásárláshoz!')</script>";
		    ?><script language="JavaScript">
                document.location.href ="cart.php";
            </script><?php
            return false;
        }else{
            vasarlas_do();
        }
    }else{
        vasarlas_do();
    }
}

function vasarlas_do(){
    $ok=true;

    $rend_id=rendeles_insert();

    if(isset($_SESSION['cart'])){
        foreach($_SESSION['cart'] as $i => $i_value) {
            $egyseg_id=next_id_rendeles_egyseg();
            $ok=rendeles_egyseg_insert($i_value, $i, $rend_id);
        }
    }

    if($ok==true){    
        kosar_clear();
        if($_POST["fiz_mod"] == "Online Egyenleg"){
            $balance=getBalance($_SESSION["user"]);
            minusBalance($balance-$_POST["purchase_amount"]);
        }
        echo "<script>alert('Sikeres rendelés leadás!')</script>";
        ?><script language="JavaScript">
                document.location.href ="purchase.php";
        </script><?php
    }else{
        echo "<script>alert('Nem sikerült a rendelést leadni!')</script>";
        ?><script language="JavaScript">
                document.location.href ="purchase.php";
        </script><?php
    }
}

function minusBalance($newBalance){
    if ( !($conn = connect() )) { 
        return false;
    }
    $stmt = mysqli_prepare( $conn, "UPDATE FELHASZNALO SET Egyenleg='" . $newBalance . "' WHERE FELHASZNALONEV='" . $_SESSION["user"] . "'");
	
    mysqli_stmt_execute($stmt);
    mysqli_close($conn);
}

function rendeles_insert(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $user_id=get_user_id();
    $r_id=next_id_rendeles();

    $stmt = mysqli_prepare($conn, "INSERT INTO RENDELES (ID, FIZ_MOD, FELH_ID) VALUES ('" . $r_id . "', '" . $_POST["fiz_mod"] . "', '" . $user_id . "')");

    if(!$sikeres=mysqli_stmt_execute($stmt)){
        mysqli_close($conn);
        clear_all($r_id);
        echo "<script>alert('Nem sikerült a rendelést létrehozni!')</script>";
        return false;
    } 
    return $r_id;
}


function rendeles_egyseg_insert($i_value, $i, $rend_id){
    if ( !($conn = connect() )) { 
        return false;
    }

    $egyseg_id=next_id_rendeles_egyseg();
    
    $stmt = mysqli_prepare($conn, "INSERT INTO RENDELESEGYSEG (ID, MENNYISEG, TERMEK_ID, RENDELES_ID) VALUES ('" . $egyseg_id . "', '" . $i_value . "', '" . $i . "', '" . $rend_id . "')");

    if(!$sikeres=mysqli_stmt_execute($stmt)){
        mysqli_close($conn);
        clear_all($rend_id);
        echo "<script>alert('Nem sikerült a rendelés egységet létrehozni!')</script>";
        return false;
    }
    return true;
}

function clear_all($r_id){
    clear_rendeles($r_id);
    clear_egyseg($r_id);
}

function clear_rendeles($r_id){
    if ( !($conn = connect() )) { 
        return false;
    	}
	
	$stmt = mysqli_prepare( $conn, "DELETE FROM RENDELES WHERE ID='" . $r_id . "'");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres clear!')</script>";
        mysqli_close($conn);
        ?><script language="JavaScript">
				document.location.href ="cart.php";
		</script><?php
	}else{
		echo "<script>alert('Sikertelen clear!')</script>";
        mysqli_close($conn);
        ?><script language="JavaScript">
				document.location.href ="cart.php";
		</script><?php
        return false;
	}
}

function clear_egyseg($r_id){
    if ( !($conn = connect() )) { 
        return false;
    	}
	
	$stmt = mysqli_prepare( $conn, "DELETE FROM RENDELESEGYSEG WHERE RENDELES_ID='" . $r_id . "'");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres egyseg clear!')</script>";
        mysqli_close($conn);
        ?><script language="JavaScript">
				document.location.href ="cart.php";
		</script><?php
	}else{
		echo "<script>alert('Sikertelen egyseg clear!')</script>";
        mysqli_close($conn);
        ?><script language="JavaScript">
				document.location.href ="cart.php";
		</script><?php
        return false;
	}
}


function next_id_rendeles(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ('SELECT MAX(ID) as max FROM RENDELES');
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($conn);
    return $row["max"] + 1;
}

function next_id_rendeles_egyseg(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ('SELECT MAX(ID) as max FROM RENDELESEGYSEG');
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($conn);
    return $row["max"] + 1;
}

function get_user_id(){
    if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT ID FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $_SESSION["user"] . "'");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    mysqli_close($conn);

    return $row["ID"];
}
?>
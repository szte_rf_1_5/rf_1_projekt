<?php

include "menu.php";

login_check_admin();

if (isset($_POST["new_hir"])) {
    hir_felvetel();
} else if (isset($_POST["delete_hir"])) {
    delete_hir();
} else if (isset($_POST["update_hir"])) {
    update_hir();
} else if (isset($_POST["mod_hir"])) {
    mod_hir();
    footer();
} else {
    hir_oldal();
    footer();
}


function hir_oldal(){
	?>
    <h2>Hírek kezelése</h2>
    </br>
    <p><strong>Új hír felvétele</strong></p>
    <form method="post" action="hir_kezeles.php">
                <p>Cím: <span><input type="text" name="cim"/></span></p>
                <p>Leíras</p>
                <textarea name="leiras" id='textarea' cols="100" rows="4"></textarea>
            </tr>
        </table>
        </br>
        <input type="submit" name="new_hir" value="Új hír felvétele" />
        </br></br>
    </form>
    
    <p><strong>Hírek listázása</strong></p>

    <?php
    	get_news();
}

function get_news(){
	if ( !($conn = connect() )) { 
        	return false;
    }

    $sql = ("SELECT ID, NEV, LEIRAS, LETREHOZ_DATUM, (select FELHASZNALONEV from FELHASZNALO WHERE FELHASZNALO.ID=HIR.FELH_ID) AS FELHASZNALO, MOD_TIMESTAMP FROM hir");
     $result = mysqli_query( $conn, $sql );

     ?>
     <table style="width: 75%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 5%;text-align: center;">ID</td>
 					<td style="width: 15%;text-align: center;">Cím</td>
 					<td style="width: 29%;text-align: center;">Leírás</td>
 					<td style="width: 10%;text-align: center;">Létrehozás</td>
 					<td style="width: 11%;text-align: center;">Létrehozó</td>
 					<td style="width: 8%;text-align: center;">Bélyeg</td>
 					<td style="width: 4%;text-align: center;">Módosítás</td>
 					<td style="width: 4%;text-align: center;">Törlés</td>
 				</tr>
 	</thead>
 	</table>
 	<?php

    while($row = mysqli_fetch_assoc($result)){
     	?>
 		<table style="width: 75%">
 			<tr>
 				<td style="width: 5%;"><?php echo $row["ID"]?></td>
 				<td style="width: 15%;"><?php echo $row["NEV"]?></td>
 				<td style="text-align: justify;width: 30%;"><?php echo $row["LEIRAS"]?></td>
 				<td style="width: 10%;"><?php echo $row["LETREHOZ_DATUM"]?></td>
 				<td style="width: 10%;"><?php echo $row["FELHASZNALO"]?></td>
 				<td style="width: 10%;"><?php echo $row["MOD_TIMESTAMP"]?></td>
 				<td style="width: 5%;">
 					<form method="post" action="hir_kezeles.php">
 						<input src="images/modify.png" style="width: 30px; height: 30px;" type="image">
 						<input type="hidden" name="mod_hir" value="<?php echo $row["ID"]?>">
					</form>
				</td>
 				<td style="width: 5%;">
 					<form method="post" action="hir_kezeles.php">
 						<input src="images/delete.png" style="width: 30px; height: 30px;" type="image">
 						<input type="hidden" name="delete_hir" value="<?php echo $row["ID"]?>">
					</form>
				</td>
 			</tr>
 		</table>
		<?php
     }
}

function mod_hir(){
	if ( !($conn = connect() )) { 
        return false;
    	}
    $sql = ("SELECT NEV, LEIRAS FROM HIR WHERE ID='" . $_POST["mod_hir"] . "'");

    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($conn);

	?>
    <h2>Hír módosítása</h2>
    </br>
    <form method="post" action="hir_kezeles.php">
                <p>Cím: <span><input type="text" name="cim" value="<?php echo $row["NEV"]?>"/></span></p>
                <p>Leíras</p>
                <textarea name="leiras" id='textarea' cols="100" rows="4"><?php echo $row["LEIRAS"]?></textarea>
            </tr>
        </table>
        </br>
        <input type="hidden" name="update_hir" value="<?php echo $_POST["mod_hir"]?>">
        <input type="submit" name="update" value="Hír módosítása">
        </br></br>
    </form>
    <?php
}

function update_hir(){
	if ( !($conn = connect() )) { 
        return false;
    	}
	
	echo $_POST["cim"]. ", ";
	echo $_POST["leiras"];
	echo $_POST["update_hir"];
	$stmt = mysqli_prepare( $conn, "UPDATE HIR SET NEV='" . $_POST["cim"] . "',  LEIRAS='" . $_POST["leiras"] . "' WHERE ID='" . $_POST["update_hir"] . "'");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres művelet!')</script>";
		?><script language="JavaScript">
				document.location.href ="hir_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="hir_kezeles.php";
		</script><?php
	}
}

function delete_hir(){
	if ( !($conn = connect() )) { 
        return false;
    	}
	
	$stmt = mysqli_prepare( $conn, "DELETE FROM HIR WHERE ID='" . $_POST["delete_hir"] . "'");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres művelet!')</script>";
		?><script language="JavaScript">
				document.location.href ="hir_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="hir_kezeles.php";
		</script><?php
	}
}

function hir_felvetel(){
	if ( !($conn = connect() )) { 
        return false;
    	}
	$id=next_id_hir();
	$stmt = mysqli_prepare($conn, "INSERT INTO HIR (ID, NEV, LEIRAS, FELH_ID) VALUES ('" . $id . "', '" . $_POST["cim"] . "', '" . $_POST["leiras"] . "', '" . $_SESSION["user_id"] . "')");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres művelet!')</script>";
		?><script language="JavaScript">
				document.location.href ="hir_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="hir_kezeles.php";
		</script><?php
	}
}


function next_id_hir(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ('SELECT MAX(ID) as max FROM HIR');
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($conn);
    return $row["max"] + 1;
}

?>
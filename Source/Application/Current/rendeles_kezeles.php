<?php

include "menu.php";

login_check_admin();

if (isset($_POST["setDone"])) {
    complete_order($_POST["setDone"]);
}

pretable();
detail_table(lekerdez());

function pretable(){
	?>
	
	<h2>Rendelések kezelése</h2>
<table style="width: 60%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 20%;text-align: left;">Rendelés ideje</td>
 					<td style="width: 20%;text-align: left;">Fizetés mód</td>
					<td style="width: 15%;text-align: left;">Mennyiségek</td>
 					<td style="width: 20%;text-align: left;">Termék(ek)</td>
 					<td style="width: 15%;text-align: left;">Összár</td>
					<td style="width: 10%;text-align: right;">Állapot</td>
 				</tr>
 	</thead>
</table>
	
	
<?php
}

function detail_table($result){
	?>
	<table style="width: 60%">
		<?php
				while ($row = mysqli_fetch_assoc($result)) {
				?>
				<tr>
				<td style="width: 20%;text-align: justify;"><?php echo $row["Rend_Datum"]; ?></td>
				<td style="width: 20%;text-align: justify;"><?php echo $row["Fiz_Mod"];?></td>
				<td style="width: 20%;text-align: justify;"><?php echo $row["DB"];?></td>
				<td style="width: 20%;text-align: left;"><?php echo $row["RENDELES"];?></td>
				<td style="width: 20%;text-align: justify;"><?php echo $row["OSSZAR"];?></td>
				<td style="width: 20%;text-align: justify;">
				<?php if($row["Teljesitve"] == 0){ ?>
				<td style="width: 10%">
						<form method="post" action="rendeles_kezeles.php">
                            <input src="images/complete.png" style="width: 30px; height: 30px;" type="image" title="Rendelés feldolgozása">
							<input type="hidden" name="setDone" value="<?php echo $row["ID"] ?>">
                        </form>
				</td>
				</tr>
				<?php
					}else{
						?>
						<td style="width: 4%;"><img src="images/done.png" style="width: 30px; height: 30px;" title="A rendelés fel van dolgozva"></td>
						</td>
						</tr>
						<?php	
				}
				}
			?>
	</table>
	<?php

}

function complete_order($order_ID){
	if (!($conn = connect() )) {
        return false;
	}
	echo $order_ID;

	$stmt = mysqli_prepare( $conn, "UPDATE rendeles SET teljesitve=1 WHERE ID='" . $order_ID . "'");
	
	if(mysqli_stmt_execute($stmt)){
		echo "<script>alert('Rendelés feldolgozva')</script>";
		?><script language="JavaScript">
				document.location.href ="rendeles_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült feldolgozni a rendelést!')</script>";
		?><script language="JavaScript">
				document.location.href ="rendeles_kezeles.php";
		</script><?php
	}
}

function lekerdez(){
	if ( !($conn = connect() )) {
        return false;
    }
	
	$sql = "SELECT Rend_Datum,Fiz_Mod,GROUP_CONCAT(nev) AS RENDELES,CONCAT(CAST(SUM(Ar*mennyiseg) AS char(20))) AS OSSZAR,GROUP_CONCAT(mennyiseg) AS DB,Teljesitve,rendeles.ID AS ID
			FROM rendeles,felhasznalo,rendelesegyseg,termek
			WHERE felhasznalo.ID = rendeles.Felh_ID AND
			rendelesegyseg.Rendeles_ID = rendeles.ID AND
			termek.ID = rendelesegyseg.Termek_ID
			GROUP BY rendeles.ID";
			
	$result = mysqli_query($conn,$sql);
	
	mysqli_close($conn);
	
	return $result;
}


?>
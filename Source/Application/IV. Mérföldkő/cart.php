<?php

include "menu.php";

if (isset($_POST["add_cart"])) {
    kosarba();
    get_kosar();
    footer();
}else if (isset($_POST["minus_cart"])){
	kosarbol();
	get_kosar();
	footer();
}else if(isset($_POST["clear_product"])){
    kosar_product_clear();
    get_kosar();
    footer();   
}else if(isset($_POST["clear_cart"])){
    kosar_clear();
    get_kosar();
    footer();
}else{
    get_kosar();
    footer();
}

function continue_purchase($osszeg){
	?>
	<form method="post" action="purchase.php">
	</br></br>
	<hr id="kisvonal" />
	<input type="hidden" name="purchase" value="<?php echo $osszeg?>">
	<input type="submit" name="continue_purchase" value="Folytatás">
	</form>
	<?php
}

function get_kosar(){
	?>
	<h2>Kosár</h2>
    <?php
    if(getCartNumber($_SESSION["cart"])>0){
        ?>
        </br>
        <h3 id="title">Termékek (<?php echo getCartNumber($_SESSION["cart"])?>)</h3>
        </br>
        </br>
        <?php
        $sum=get_details();
        continue_purchase($sum);
    }else{
        echo "<h3>Jelenleg nincs a kosárban termék!</h3>";
    }
}   

function get_details(){
    pretable();
    $sum=0;
	$sum=collect_data();
	return $sum;
}

function pretable(){
	?>
     <table style="width: 60%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 7%;text-align: left;">Kép</td>
 					<td style="width: 9%;text-align: left;">Név</td>
 					<td style="width: 3%;text-align: left;">Kiszerelés</td>
 					<td style="width: 3%;text-align: center;">Akció</td>
                    <td style="width: 5%;text-align: center;">Ár</td>
                    <td style="width: 5%;text-align: center;">Összesen</td>
                    <td style="width: 3%;text-align: right;">-</td>
                    <td style="width: 5%;text-align: right;">Mennyiség</td>
 					<td style="width: 3%;text-align: right;">+</td>
                    <td style="width: 3%;text-align: right;">Törlés</td>
 				</tr>
 	</thead>
 	</table>
 	<?php
}

function collect_data(){
    $sum=0;
    foreach($_SESSION['cart'] as $i => $i_value) {
        $sum+=get_spec_product($i, $i_value);
    }
    ?>
    </br>
    </br>
    <h3>Fizetendő: <?php echo number_format($sum,0,".",".")?> Ft</h3>
    <?php
	return $sum;
}

function get_spec_product($key, $count){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ("SELECT ID, NEV, KISZERELES, AR, SRC, AKCIO FROM TERMEK WHERE ID='" . $key . "'");
    $result = mysqli_query( $conn, $sql );
    
    $price=0;

    while($row = mysqli_fetch_assoc($result)){
        detail_table_cart($row, $count);
        $price+=$row["AR"]*$count;
    }
    return $price;
}

function detail_table_cart($row, $count){
    ?>
	<table style="width: 60%">
		<tr>
			<td style="width: 7%;text-align: left;">
			<?php
				if(!is_null($row["SRC"])){
					?>
					<img src="images/<?php echo $row["SRC"]?>" style="float:left;width: 100px; height: 100px;">
					<?php
				}else{
					?>
					<img src="images/soon.png" style="float:left;width: 100px; height: 100px;">
					<?php
				}
			?>
			</td>
			<td style="width: 13%;text-align: justify;"><?php echo $row["NEV"]?></td>
			<td style="width: 8%;text-align: left;"><?php echo $row["KISZERELES"]?></td>
			<td style="width: 4%;text-align: left;">
				<?php
				if($row["AKCIO"]){
					?>
						<img src="images/discount.png" style="width: 30px; height: 30px;" title="25% kedvezmény">
					<?php
				}
				?>	
			</td>
            <td style="width: 7%;text-align: center;"><?php echo $row["AR"]." Ft"?></td>
			<td style="width: 9%;text-align: center;"><b><?php echo number_format($row["AR"]*$count,0,".",".")?> Ft</b></td>
            <td style="width: 6%;">
				<form method="post" action="cart.php">
					<input src="images/minus.png" style="width: 30px; height: 30px;" type="image" title="Mennyiség csökkentése">
					<input type="hidden" name="minus_cart" value="<?php echo $row["ID"]?>">
				</form>
			</td>
            <td style="width: 7%;text-align: center;"><?php echo $count." db"?></td>
            <td style="width: 6%;">
				<form method="post" action="cart.php">
					<input src="images/plus.png" style="width: 30px; height: 30px;" type="image" title="Mennyiség növelése">
					<input type="hidden" name="add_cart" value="<?php echo $row["ID"]?>">
				</form>
			</td>
            <td style="width: 3%;">
				<form method="post" action="cart.php">
					<input src="images/delete.png" style="width: 30px; height: 30px;" type="image" title="Termék(ek) törlése a kosárból">
					<input type="hidden" name="clear_product" value="<?php echo $row["ID"]?>">
				</form>
			</td>
		</tr>
	</table>
    <?php
}
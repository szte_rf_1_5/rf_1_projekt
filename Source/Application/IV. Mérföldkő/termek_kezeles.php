<?php

include "menu.php";

login_check_admin();

if (isset($_POST["new_termek"])) {
    new_termek();
}else if (isset($_POST["delete_termek"])) {
    delete_termek();
} else if (isset($_POST["update_termek"])) {
    update_termek();
} else if (isset($_POST["modify_termek"])) {
    modify_termek();
    footer();
}else{
	termekek_admin_oldal();
	footer();
}

function new_termek(){
	if(!isset($_POST["nev"]) || !isset($_POST["kiszereles"]) || !isset($_POST["ar"]) || 
		!isset($_POST["akcio"]) || !isset($_POST["leiras"]) || !isset($_POST["alkat_id"])){
		echo "<script>alert('Hiányzó adatok a felvétel során!')</script>";
		?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
		</script><?php
	}elseif(!is_numeric($_POST["ar"]) || !is_numeric($_POST["alkat_id"])){
		echo "<script>alert('Invalid alkategória vagy ár!')</script>";
		?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
		</script><?php
	}else{
		if ( !($conn = connect() )) { 
	        return false;
	    }
		$id=next_id_termek();
		$stmt = mysqli_prepare($conn, "INSERT INTO TERMEK (ID, NEV, KISZERELES, AR, AKCIO, LEIRAS, ALKAT_ID) VALUES ('" . $id . "', '" . $_POST["nev"] . "', '" . $_POST["kiszereles"] . "', '" . $_POST["ar"] . "', '" . $_POST["akcio"] . "', '" . $_POST["leiras"] . "', '" . $_POST["alkat_id"] . "')");
	
		if($sikeres=mysqli_stmt_execute($stmt)){
			echo "<script>alert('Sikeres művelet!')</script>";
			?><script language="JavaScript">
					document.location.href ="termek_kezeles.php";
			</script><?php
		}else{
			echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
			?><script language="JavaScript">
					document.location.href ="hir_kezeles.php";
			</script><?php
		}
	}
}

function next_id_termek(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ('SELECT MAX(ID) as max FROM TERMEK');
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($conn);
    return $row["max"] + 1;
}

function delete_termek(){
	if ( !($conn = connect() )) { 
        return false;
    	}
	
	$stmt = mysqli_prepare( $conn, "DELETE FROM TERMEK WHERE ID='" . $_POST["delete_termek"] . "'");
	
	if($sikeres=mysqli_stmt_execute($stmt)){
		echo "<script>alert('Sikeres művelet!')</script>";
		?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
		</script><?php
	}else{
		echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
		?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
		</script><?php
	}
}


function modify_termek(){
		if ( !($conn = connect() )) { 
	        return false;
	    }

	    $sql = ("SELECT nev, kiszereles, ar, akcio, leiras, alkat_id FROM TERMEK WHERE ID='" . $_POST["modify_termek"] . "'");

	    $result = mysqli_query( $conn, $sql );
	    $row = mysqli_fetch_assoc($result);
	    mysqli_close($conn);

	 	?>
    	<h2>Termék módosítása</h2>
   		</br>
   		<form method="post" action="termek_kezeles.php">
         <table>
            <tr>
                <td>Név</td>
                <td><input type="text" name="nev" value="<?php echo $row["nev"]?>"/></td>
            </tr>
            <tr>
                <td>Kiszerelés</td>
                <td><input type="text" name="kiszereles" value="<?php echo $row["kiszereles"]?>"/></td>
            </tr>
            <tr>
                <td>Ár</td>
                <td><input type="text" name="ar" value="<?php echo $row["ar"]?>"/></td>
            </tr>
            <tr>
                <td>Akció</td>
                <?php
                if($row["akcio"]=='0'){
                	?>
                	<td><input type="radio" name="akcio" value="0" checked="checked"/>Nem
                	<input type="radio" name="akcio" value="1" />Igen</td>
                	<?php
                }else {
                	?>
                	<td><input type="radio" name="akcio" value="0"/>Nem
                	<input type="radio" name="akcio" value="1" checked="checked"/>Igen</td>
                	<?php
                }
                ?>
            </tr>
             <tr>
                <td>Leírás</td>
                <td><input type="text" name="leiras" value="<?php echo $row["leiras"]?>"/></td>
            </tr>
            <tr>
                <td>Alkategória</td>
                <td>
                    <select name="alkat_id">
	    				<option value="<?php echo $row["alkat_id"]?>"> Válassz kategóriát!</option>
	    				<?php 
	    					get_category();
	   					?>
    				</select>
                </td>
            </tr>
        </table>
        </br>
        <input type="hidden" name="update_termek" value="<?php echo $_POST["modify_termek"]?>">
        <input type="submit" name="update" value="Termék módosítása">
        </br></br>
    </form>
    <?php
}

function update_termek(){
	if(!isset($_POST["nev"]) || !isset($_POST["kiszereles"]) || !isset($_POST["ar"]) || 
		!isset($_POST["akcio"]) || !isset($_POST["leiras"]) || !isset($_POST["alkat_id"])){
			echo "<script>alert('Hiányzó adatok a felvétel során!')</script>";
		?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
		</script><?php
	}elseif(!is_numeric($_POST["ar"]) || !is_numeric($_POST["alkat_id"])){
		/*echo "<script>alert('Invalid alkategória vagy ár!')</script>";
		?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
		</script><?php*/
		echo $_POST["ar"];
		echo $_POST["alkat_id"];
	}else{
		if ( !($conn = connect() )) { 
	        return false;
	    }

	    $stmt = mysqli_prepare( $conn, "UPDATE TERMEK SET NEV='" . $_POST["nev"] . "',  KISZERELES='" . $_POST["kiszereles"] . "',  AR='" . $_POST["ar"] . "',  AKCIO='" . $_POST["akcio"] . "',  LEIRAS='" . $_POST["leiras"] . "',  ALKAT_ID='" . $_POST["alkat_id"] . "' WHERE ID='" . $_POST["update_termek"] . "'");

	    if($sikeres=mysqli_stmt_execute($stmt)){
			echo "<script>alert('Sikeres művelet!')</script>";
			?><script language="JavaScript">
				document.location.href ="termek_kezeles.php";
			</script><?php
		}else{
			echo "<script>alert('Nem sikerült a műveletet végrehajtani!')</script>";
			?><script language="JavaScript">
			document.location.href ="termek_kezeles.php";
			</script><?php
		}
	}
}

function termekek_admin_oldal(){
	?>
    <h2>Termékek kezelése</h2>
    </br>
    <p><strong>Új termék felvétele</strong></p>
    <form method="post" action="termek_kezeles.php">
         <table>
            <tr>
                <td>Név</td>
                <td><input type="text" name="nev"/></td>
            </tr>
            <tr>
                <td>Kiszerelés</td>
                <td><input type="text" name="kiszereles"/></td>
            </tr>
            <tr>
                <td>Ár</td>
                <td><input type="text" name="ar"/></td>
            </tr>
            <tr>
                <td>Akció</td>
                <td><input type="radio" name="akcio" value="0" checked="checked"/>Nem
                <input type="radio" name="akcio" value="1" />Igen</td>
            </tr>
             <tr>
                <td>Leírás</td>
                <td><input type="text" name="leiras"/></td>
            </tr>
            <tr>
                <td>Alkategória</td>
                <td>
                    <select name="alkat_id">
	    				<option value=""> Válassz kategóriát!</option>
	    				<?php 
	    					get_category();
	   					?>
    				</select>
                </td>
            </tr>
        </table>
        </br>
        <input type="submit" name="new_termek" value="Új termék felvétele" />
        </br></br>
    </form>
    
    <p><strong>Termékek listázása</strong></p>

    <?php
    	get_termekek();
}

function get_category(){
	if ( !($conn = connect() )) { 
        return false;
    }

	$sql = ("SELECT NEV, ID FROM ALKATEGORIA");
			$result = mysqli_query( $conn, $sql );
	 
	while($row = mysqli_fetch_array($result)){
	        echo "<OPTION VALUE='".$row["ID"]."'>".$row["NEV"]."</OPTION>";
	}
}

function get_termekek(){
	?>
	</br>
    <h3 id="title">Ételek</h3>
	    	<?php
	    		get_pizzak();
	    		get_gyrosok();
	    		get_hambik();
	    		get_koretek();
	    	?>
	    </br>
    <h3 id="title">Italok</h3>
	    	<?php
	    		get_szensavas();
	    		get_mentes();
	    	?>
	<?php
}

function count_test($alkat){
	if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(ID) AS num FROM TERMEK WHERE ALKAT_ID = (SELECT ID FROM ALKATEGORIA WHERE NEV='" . $alkat . "')");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    if ($row["num"] == 0) {
        mysqli_close($conn);
        return false;
    } else {
        mysqli_close($conn);
        return true;
    }
}

function count_alkat($alkat){
	if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(ID) AS num FROM TERMEK WHERE ALKAT_ID = (SELECT ID FROM ALKATEGORIA WHERE NEV='" . $alkat . "')");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    mysqli_close($conn);
    
    return $row["num"];
}


function pretable(){
	?>
     <table style="width: 60%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 5%;text-align: left;">ID</td>
 					<td style="width: 12%;text-align: left;">Kép</td>
 					<td style="width: 14%;text-align: left;">Név</td>
 					<td style="width: 9%;text-align: left;">Kiszerelés</td>
 					<td style="width: 35%;text-align: left;">Leírás</td>
 					<td style="width: 4%;text-align: left;">Ár</td>
 					<td style="width: 10%;text-align: right;">Bélyeg</td>
 					<td style="width: 6%;text-align: right;">Akció</td>
 					<td style="width: 6%;text-align: right;">Módosítás</td>
 					<td style="width: 10%;text-align: right;">Törlés</td>
 				</tr>
 	</thead>
 	</table>
 	<?php
}

function detail_table($row){
	?>
	<table style="width: 60%">
		<tr>
			<td style="width: 4%;text-align: left;"><?php echo $row["ID"]?></td>
			<td style="width: 10%;text-align: left;">
			<?php
				if(!is_null($row["SRC"])){
					?>
					<img src="images/<?php echo $row["SRC"]?>" style="float:left;width: 100px; height: 100px;">
					<?php
				}else{
					?>
					<img src="images/soon.png" style="float:left;width: 100px; height: 100px;">
					<?php
				}
			?>
			</td>
			<td style="width: 12%;text-align: justify;"><?php echo $row["NEV"]?></td>
			<td style="width: 7%;text-align: left;"><?php echo $row["KISZERELES"]?></td>
			<td style="text-align: justify;width: 25%;"><?php echo $row["LEIRAS"]?></td>
			<td style="width: 7%;text-align: left;"><?php echo $row["AR"]." Ft"?></td>
			<td style="width: 8%;text-align: right;"><?php echo $row["MOD_TIMESTAMP"]?></td>
			<td style="width: 5%;text-align: center;">
				<?php
				if($row["AKCIO"]){
					?>
						<img src="images/discount.png" style="width: 30px; height: 30px;" title="25% kedvezmény">
					<?php
				}
				?>	
			</td>
			<td style="width: 7%;">
				<form method="post" action="termek_kezeles.php">
					<input src="images/modify.png" style="width: 30px; height: 30px;" type="image">
					<input type="hidden" name="modify_termek" value="<?php echo $row["ID"]?>">
				</form>
			</td>
			<td style="width: 5%;">
				<form method="post" action="termek_kezeles.php">
					<input src="images/delete.png" style="width: 30px; height: 30px;" type="image">
					<input type="hidden" name="delete_termek" value="<?php echo $row["ID"]?>">
				</form>
			</td>
		</tr>
	</table>
	<?php
}

function get_pizzak(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Pizza")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO, MOD_TIMESTAMP FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Pizza')");
	    $result = mysqli_query( $conn, $sql );

		?>
		<hr id="kisvonal" />
		   <p>Pizzák (<?php echo count_alkat("Pizza");?>)</p>  
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_gyrosok(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Gyros")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO, MOD_TIMESTAMP FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Gyros')");
	    $result = mysqli_query( $conn, $sql );
		 
		?>
		<hr id="kisvonal" />
		   <p>Gyrosok (<?php echo count_alkat("Gyros");?>)</p>
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_hambik(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Hamburger")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO, MOD_TIMESTAMP FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Hamburger')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Hamburgerek (<?php echo count_alkat("Hamburger");?>)</p>
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_koretek(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Köret")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO, MOD_TIMESTAMP FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Köret')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Köretek (<?php echo count_alkat("Köret");?>)</p>
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}

}

function get_szensavas(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Szénsavas")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO, MOD_TIMESTAMP FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Szénsavas')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Szénsavas (<?php echo count_alkat("Szénsavas");?>)</p>
		 <?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_mentes(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Szénsavmentes")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO, MOD_TIMESTAMP FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Szénsavmentes')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Szénsavmentes (<?php echo count_alkat("Szénsavmentes");?>)</p>
		 <?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

?>
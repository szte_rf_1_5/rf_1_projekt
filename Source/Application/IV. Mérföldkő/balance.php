<?php

include "menu.php";

login_check();

if (isset($_POST["add_balance"])) {
    add_balance();
} else {
    display_form();
}

function display_form(){
    ?>
    <h2>Egyenleg feltöltése</h2>
    </br>
    <h3>Jelenlegi egyenleg: <b><?php echo number_format(getBalance($_SESSION["user"]),0,".",".")?> Ft</b></h3>
    
    <p>Elfogadott kártyatípus:<p>

    <img src="images/mastercard.png" width="100px" height="100px" title="MasterCard">

    <p style="color:red">A csillaggal jelölt mezők kitöltése kötelező!</p>
    <p style="color:red">A kártya számát egyben add meg!</p>
    
    <form method="post" action="balance.php">
    <table>
        <tr>
            <td>Feltöltendő összeg<b style="color:red">*</b></td>
            <td><input type="text" name="amount" maxlength="6"></td>
        </tr>
        <tr>
            <td>Vezetéknév<b style="color:red">*</b></td>
            <td><input type="text" name="vez_nev"></td>
        </tr>
        <tr>
            <td>Keresztnév<b style="color:red">*</b></td>
            <td><input type="text" name="ker_nev"></td>
        </tr>
        <tr>
            <td>Kártyaszám<b style="color:red">*</b></td>
            <td><input type="text" name="card_num" maxlength="16" placeholder="xxxxxxxxxxxxxxxx"></td>
        </tr>
        <tr>
            <td>CVV/CVC<b style="color:red">*</b></td>
            <td><input type="password" name="cvv" maxlength="3" placeholder="xxx"></td>
        </tr>
        <tr>
            <td>Lejárati Hónap<b style="color:red">*</b></td>
            <td>
                <select name="exp_month">
                    <option value="01">Január</option>
                    <option value="02">Február</option>
                    <option value="03">Március</option>
                    <option value="04">Április</option>
                    <option value="05">Május</option>
                    <option value="06">Június</option>
                    <option value="07">Július</option>
                    <option value="08">Augusztus</option>
                    <option value="09">Szeptember</option>
                    <option value="10">Október</option>
                    <option value="11">November</option>
                    <option value="11">December</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Lejárat Év<b style="color:red">*</b></td>
            <td>
                <select name="exp_year">
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                </select>
            </td>
        </tr>
    </table>
    </br>
    <input type="submit" name="add_balance" value="Egyenleg feltöltése"/>
    </br></br>
    </form>
<?php
}

function add_balance(){
    if(!check_values()){
        return;
    }

    $actualBalance=getBalance($_SESSION["user"]);

    $refresh=refresh_balance($actualBalance+$_POST["amount"]);

    if($refresh){
        echo "<script>alert('Sikeres egyenleg feltöltés!')</script>";
        ?>
        <script language="JavaScript">
            document.location.href ="balance.php";
        </script>
        <?php
    }else{
        echo "<script>alert('A műveletet nem sikerült végrehajtani!')</script>";
        ?>
        <script language="JavaScript">
            document.location.href ="balance.php";
        </script>
        <?php
    }
}

function check_values(){
    if ((($_POST["amount"]) == "" || ($_POST["vez_nev"]) == "" || ($_POST["ker_nev"]) == "" || ($_POST["card_num"]) == "" || ($_POST["cvv"]) == ""  || ($_POST["exp_month"]) == ""  || ($_POST["exp_year"]) == "")) {
        echo "<script>alert('Hiányzó adatok!')</script>";
        ?><script language="JavaScript">
                document.location.href ="balance.php";
        </script><?php
        return false;
    }
    if (!is_numeric($_POST["amount"])) {
        echo "<script>alert('Hibás mennyiség!')</script>";
        ?><script language="JavaScript">
                document.location.href ="balance.php";
        </script><?php
        return false;
    }
    if (!is_numeric($_POST["card_num"])) {
        echo "<script>alert('A kártyaszám csak szám lehet!')</script>";
        ?><script language="JavaScript">
                document.location.href ="balance.php";
        </script><?php
        return false;
    }
    if (strlen($_POST["card_num"])!=16) {
        echo "<script>alert('A kártyaszám 16 számból áll!')</script>";
        ?><script language="JavaScript">
                document.location.href ="balance.php";
        </script><?php
        return false;
    }
    if (!is_numeric($_POST["cvv"])) {
        echo "<script>alert('Az cvv csak szám lehet!')</script>";
        ?><script language="JavaScript">
               document.location.href ="balance.php";
        </script><?php
        return false;
    }
    if (strlen($_POST["cvv"])!=3) {
        echo "<script>alert('A CVC/CCV 3 számból áll!')</script>";
        ?><script language="JavaScript">
                document.location.href ="balance.php";
        </script><?php
        return false;
    }
    return true;
}


function refresh_balance($newBalance){
    if ( !($conn = connect() )) { 
        return false;
    }
    $stmt = mysqli_prepare( $conn, "UPDATE FELHASZNALO SET Egyenleg='" . $newBalance . "' WHERE FELHASZNALONEV='" . $_SESSION["user"] . "'");
	
    if(mysqli_stmt_execute($stmt)){
        mysqli_close($conn);
        return true;
    }else{
        mysqli_close($conn);
        return false;
    }
}

?>


            

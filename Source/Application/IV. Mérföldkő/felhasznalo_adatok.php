<?php

include "menu.php";

login_check();

if (isset($_POST["modify"])) {
    modify_user();
} else {
    user_data();
}


function user_data(){
?>
    <h2><?php echo $_SESSION["user"]?> felhasználó adatai</h2>
    <p>A csillaggal jelölt mezők kitöltése kötelező!<p>

    <?php 
    $user_details=get_user_data();
    ?>
    <form method="post" action="felhasznalo_adatok.php">
        <table>
            <tr>
                <td>E-mail<b style="color:red">*</b></td>
                <td><input type="text" name="email" value=<?php echo $user_details["Email"]?>></td>
            </tr>
                <tr>
                <td>Vezetéknév</td>
                <td><input type="text" name="vez" value=<?php echo $user_details["Vez_nev"]?>></td>
            </tr>
            <tr>
                <td>Keresztnév</td>
                <td><input type="text" name="ker" value=<?php echo $user_details["Ker_nev"]?>></td>
            </tr>
            <tr>
                <td>Nem</td>
                <?php 
                if($user_details["Nem"]=="F"){
                    ?>
                    <td>
                    <select name="nem" id="nem">
                        <option selected value="F">Férfi</option>
                        <option value="N">Nő</option>
                    </select>
                    </td>
                    <?php
                }else if($user_details["Nem"]=="N"){
                    ?>
                    <td>
                    <select name="nem" id="nem">
                        <option value="F">Férfi</option>
                        <option selected value="N">Nő</option>
                    </select>
                    </td>
                    <?php
                }else{
                    ?>
                    <td>
                        <select name="nem" id="nem">
                            <option selected>Valaszd ki a nemed!</option>
                            <option value="F">Férfi</option>
                            <option value="N">Nő</option>
                        </select>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <tr>
                <td>Születési dátum</td>
                <td><input type="date" name="date" id="date" value=<?php echo $user_details["Szulido"]?>></td>
            </tr>
            <tr>
                <td>Facebook</td>
                <td><input type="text" name="facebook" placeholder="http://facebook.com/felhasznalonev" value=<?php echo $user_details["Fb_url"]?>></td>
            </tr>
            <tr>
                <td>Irányítószám<b style="color:red">*</b></td>
                <td><input type="text" name="irszam" value=<?php echo $user_details["Varos_ZIP"]?>></td>
            </tr>
            <tr>
                <td>Város<b style="color:red">*</b></td>
                <td><input type="text" name="varos" value=<?php echo get_city_name($user_details["Varos_ZIP"])?>></td>
            </tr>
            <tr>
                <td>Utca<b style="color:red">*</b></td>
                <td><input type="text" name="utca" value=<?php echo $user_details["Utca"]?>></td>
            </tr>
            <tr>
                <td>Házszám<b style="color:red">*</b></td>
                <td><input type="text" name="hazszam" value=<?php echo $user_details["Hazszam"]?>></td>
            </tr>
        </table>
        </br>
        <input type="submit" name="modify" value="Adatok módosítása"/>
        </br></br>
    </form>
<?php
}


function get_user_data(){
    if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT Email, Vez_nev, Ker_nev, Nem, Szulido, Fb_url, Varos_ZIP, Utca, Hazszam FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $_SESSION["user"] . "'");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    return $row;
}


function get_city_name($irszam){
    if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT nev FROM varos WHERE zip = '" . $irszam . "'");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    mysqli_close($conn);

    return $row["nev"];
}

function irszam_test($irszam) {
    if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(1) AS num FROM varos WHERE zip = '" . $irszam . "'");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);

    if ($row["num"] == 0) {
        mysqli_close($conn);
        return false;
    } else {
        mysqli_close($conn);
        return true;
    }
}

function add_city($irszam, $varos){
    if ( !($conn = connect() )) { 
        return false;
    }
    $stmt = mysqli_prepare($conn, "INSERT INTO VAROS (ZIP, NEV) VALUES ('" . $irszam . "','" . $varos . "')");
    $sikeres=mysqli_stmt_execute($stmt);
    mysqli_close($conn);
}

function save_data($email, $vez, $ker, $nem, $date, $facebook, $irszam, $utca, $hazszam){
    if ( !($conn = connect() )) { 
        return false;
    }
    $stmt = mysqli_prepare($conn, "UPDATE FELHASZNALO SET Email='" . $email . "', Vez_nev='" . $vez . "', 
                                    Ker_nev='" . $ker . "', Nem='" . $nem . "', Szulido='" . $date . "', 
                                    Fb_url='" . $facebook . "', Varos_ZIP='" . $irszam . "', 
                                    Utca='" . $utca . "', Hazszam='" . $hazszam . "' 
                                    WHERE FELHASZNALONEV = '" . $_SESSION["user"] . "'");
    $sikeres=mysqli_stmt_execute($stmt);
    mysqli_close($conn);

    return $sikeres;
}

function modify_user(){
    $email = ($_POST["email"]);
    $vez = ($_POST["vez"]);
    $ker = ($_POST["ker"]);
    if (($_POST["nem"]) != "Valaszd ki a nemed!") {
        $nem = ($_POST["nem"]);
    } else {
        $nem = "";
    }
    $date = ($_POST["date"]);
    $facebook = ($_POST["facebook"]);
    $irszam = ($_POST["irszam"]);
    $varos = ($_POST["varos"]);
    $utca = ($_POST["utca"]);
    $hazszam = ($_POST["hazszam"]);

    if(!check_values()){
        return;
    }

    if(!irszam_test($irszam)){
        add_city($irszam, $varos);
    }

    $sikeres=save_data($email, $vez, $ker, $nem, $date, $facebook, $irszam, $utca, $hazszam);

    if($sikeres){
        echo "<script>alert('Sikeresen módosítás!')</script>";
        ?>
        <script language="JavaScript">
            document.location.href ="felhasznalo_adatok.php";
        </script>
        <?php
    }else{
        echo "<script>alert('A műveletet nem sikerült végrehajtani!')</script>";
        ?>
        <script language="JavaScript">
            document.location.href ="felhasznalo_adatok.php";
        </script>
        <?php
    }
}

function check_values(){
    if ((($_POST["email"]) == "" || ($_POST["irszam"]) == "" || ($_POST["varos"]) == "" || ($_POST["utca"]) == "" || ($_POST["hazszam"]) == "")) {
        echo "<script>alert('Hiányzó adatok!')</script>";
        ?><script language="JavaScript">
                document.location.href ="felhasznalo_adatok.php";
        </script><?php
        return false;
    }
    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        echo "<script>alert('Hibás E-mail cím!')</script>";
        ?><script language="JavaScript">
                document.location.href ="felhasznalo_adatok.php";
        </script><?php
        return false;
    }
    if (!is_numeric($_POST["hazszam"])) {
        echo "<script>alert('A házszám csak szám lehet!')</script>";
        ?><script language="JavaScript">
                document.location.href ="felhasznalo_adatok.php";
        </script><?php
        return false;
    }
    if (!is_numeric($_POST["irszam"])) {
        echo "<script>alert('Az irányítószám csak szám lehet!')</script>";
        ?><script language="JavaScript">
                document.location.href ="felhasznalo_adatok.php";
        </script><?php
        return false;
    }
    return true;
}
?>
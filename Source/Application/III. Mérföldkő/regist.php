<?php
include "menu.php";

if (isset($_POST["username"])) {
    regist_do();
} else if (isset($_SESSION["user"])) {
    header("Location: index.php");
} else {
    regist_data("");
}

function regist_data($error){	
        ?>
        <h2>Regisztráció</h2>
        <p>A csillaggal jelölt mezők kitöltése kötelező!<p>
        <?php
        if ($error!=""){
            ?>
            <span style="color:red">
            <?php
            echo $error;
            ?>
            </span></br>
        <?php
        }
        ?>
        <form method="post" action="regist.php">
            <table>
                <tr>
                    <td>Felhasználónév<b style="color:red">*</b></td>
                    <td><input type="text" name="username"/></td>
                </tr>
                <tr>
                    <td>Jelszó<b style="color:red">*</b></td>
                    <td><input type="password" name="password"/></td>
                </tr>
                <tr>
                    <td>Jelszó ismét<b style="color:red">*</b></td>
                    <td><input type="password" name="password2"/></td>
                </tr>
                <tr>
                    <td>E-mail<b style="color:red">*</b></td>
                    <td><input type="text" name="email"/></td>
                </tr>
                 <tr>
                    <td>Vezetéknév</td>
                    <td><input type="text" name="vez"/></td>
                </tr>
                <tr>
                    <td>Keresztnév</td>
                    <td><input type="text" name="ker"/></td>
                </tr>
                <tr>
                    <td>Nem</td>
                    <td>
                        <select name="nem" id="nem">
                            <option selected>Valaszd ki a nemed!</option>
                            <option value="F">Férfi</option>
                            <option value="N">Nő</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Születési dátum</td>
                    <td><input type="date" name="date" id="date"/></td>
                </tr>
                <tr>
                    <td>Facebook</td>
                    <td><input type="text" name="facebook" placeholder="http://facebook.com/felhasznalonev"/></td>
                </tr>
                <tr>
                    <td>Irányítószám<b style="color:red">*</b></td>
                    <td><input type="text" name="irszam"/></td>
                </tr>
                <tr>
                    <td>Város<b style="color:red">*</b></td>
                    <td><input type="text" name="varos"/></td>
                </tr>
                <tr>
                    <td>Utca<b style="color:red">*</b></td>
                    <td><input type="text" name="utca"/></td>
                </tr>
                <tr>
                    <td>Házszám<b style="color:red">*</b></td>
                    <td><input type="text" name="hazszam"/></td>
                </tr>
            </table>
            </br>
            <input type="submit" name="regist" value="Regisztráció"/>
            </br></br>
        </form>
    <?php
}

function regist_do()
{
    $username = ($_POST["username"]);
    $password = ($_POST["password"]);
    $password2 = ($_POST["password2"]);
    $email = ($_POST["email"]);
    $vez = ($_POST["vez"]);
    $ker = ($_POST["ker"]);
    if (($_POST["nem"]) != "Valaszd ki a nemed!") {
        $nem = ($_POST["nem"]);
    } else {
        $nem = "";
    }
    $date = ($_POST["date"]);
    $facebook = ($_POST["facebook"]);
    $irszam = ($_POST["irszam"]);
    $varos = ($_POST["varos"]);
    $utca = ($_POST["utca"]);
    $hazszam = ($_POST["hazszam"]);

    if (($_POST["username"]) == "" || ($_POST["password"]) == "" || ($_POST["password2"]) == "" || ($_POST["email"]) == "" || ($_POST["irszam"]) == "" || ($_POST["varos"]) == "" || ($_POST["utca"]) == "" || ($_POST["hazszam"]) == "") {
        regist_data("Hiányzó adatok!");
        return;
    }
    if ($password != $password2) {
        regist_data("A két jelszó nem egyezik meg!");
        return;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        regist_data("Az e-mail cím nem valid!");
        return;
    }

    if (user_test($username)) {
        regist_data("A felhasználónév már foglalt!");
        return;
    }

    if (!is_numeric($hazszam)) {
        regist_data("A házszám csak szám lehet!");
        return;
    }

    if (!is_numeric($irszam)) {
        regist_data("Az irányítószám csak szám lehet!");
        return;
    }

    register($username, $password, $email, $vez, $ker, $nem, $date, $facebook, $irszam, $varos, $utca, $hazszam);

    echo "<script>alert('Sikeres regisztráció, jelentkezz be!')</script>";
    ?>
    <script language="JavaScript">
    document.location.href ="login.php";
    </script>
    <?php
}

function register($username, $password, $email, $vez, $ker, $nem, $date, $facebook, $irszam, $varos, $utca, $hazszam) {
    if(!irszam_test($irszam)){
			add_city($irszam, $varos);
    }
	//$password = sha1($password);
    add_user($username, $password, $email, $vez, $ker, $nem, $date, $facebook, $irszam, $utca, $hazszam);

	$_SESSION["email"]=$email;
	$_SESSION["username"]=$username;
	$_SESSION["nev"]=$vez." ".$ker;

}

function user_test($Username) {
        if ( !($conn = connect() )) { 
        return false;
        }

        $sql = ("SELECT count(FELHASZNALONEV) AS num FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $Username . "'");
        $result = mysqli_query( $conn, $sql );
        $row = mysqli_fetch_assoc($result);
        
        if ($row["num"] == 0) {
            mysqli_close($conn);
            return false;
        } else {
            mysqli_close($conn);
            return true;
        }
}

function next_id_felhasznalo(){
    if ( !($conn = connect() )) { 
        return false;
    }
    $sql = ('SELECT MAX(ID) as max FROM FELHASZNALO');
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($conn);
    return $row["max"] + 1;
}

function irszam_test($irszam) {
        if ( !($conn = connect() )) { 
        return false;
        }

        $sql = ("SELECT count(1) AS num FROM varos WHERE zip = '" . $irszam . "'");
        $result = mysqli_query( $conn, $sql );
        $row = mysqli_fetch_assoc($result);
        
        if ($row["num"] == 0) {
            mysqli_close($conn);
            return false;
        } else {
            mysqli_close($conn);
            return true;
        }
}

function add_city($irszam, $varos){
    if ( !($conn = connect() )) { 
        return false;
    }
    $stmt = mysqli_prepare($conn, "INSERT INTO VAROS (ZIP, NEV) VALUES ('" . $irszam . "','" . $varos . "')");
    $sikeres=mysqli_stmt_execute($stmt);
    mysqli_close($conn);
}


function add_user($username, $password, $email, $vez, $ker, $nem, $date, $facebook, $irszam, $utca, $hazszam){
    if ( !($conn = connect() )) { 
        return false;
    }
    $id=next_id_felhasznalo();
    $stmt = mysqli_prepare($conn, "INSERT INTO FELHASZNALO (ID, FELHASZNALONEV, JELSZO, EMAIL, VEZ_NEV, KER_NEV, NEM, SZULIDO, FB_URL, VAROS_ZIP, UTCA, HAZSZAM) VALUES( '" . $id . "', '" . $username . "', '" . $password . "', '" . $email . "', '" . $vez . "', '" . $ker . "', '" . $nem . "', '" . $date . "', '" . $facebook . "', '" . $irszam . "', '" . $utca . "', '" . $hazszam . "')");
    $sikeres=mysqli_stmt_execute($stmt);
    mysqli_close($conn);
}

?>
CREATE DATABASE eMeal COLLATE=utf8_hungarian_ci;

CREATE TABLE Kategoria (
	ID int(6) NOT NULL,
	Nev varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Kategoria
	ADD PRIMARY KEY (ID);
	
INSERT INTO Kategoria (ID, Nev) VALUES 
	(1, 'Étel'), 
	(2, 'Ital');



CREATE TABLE Alkategoria (
	ID int(6) NOT NULL,
	Nev varchar(50) NOT NULL,
	Kat_ID int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Alkategoria
	ADD PRIMARY KEY (ID),
	ADD KEY Kat_ID_Alkategoria (Kat_ID),
	ADD CONSTRAINT Kat_ID_Alkategoria FOREIGN KEY (Kat_ID) REFERENCES Kategoria (ID) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Alkategoria (ID, Nev, Kat_ID) VALUES
	(1, 'Pizza', 1),
	(2, 'Gyros', 1),
	(3, 'Hamburger', 1),
	(4, 'Köret', 1),
	(5, 'Szénsavas', 2),
	(6, 'Szénsavmentes', 2),
	(7, 'Rostos', 2);



CREATE TABLE Varos (
	ZIP int(4) NOT NULL,
	Nev varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Varos
	ADD PRIMARY KEY (ZIP);
	
INSERT INTO Varos (ZIP, Nev) VALUES
	(6783, 'Ásotthalom');



CREATE TABLE Termek (
	ID int(6) NOT NULL,
	Nev varchar(30) NOT NULL,
	Kiszereles varchar(30) NOT NULL,
	Ar int(6) NOT NULL,
	Akcio boolean DEFAULT 0,
	Leiras varchar(255) NOT NULL,
	Src varchar(100) DEFAULT NULL,
	Mod_Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	Alkat_ID int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Termek
	ADD PRIMARY KEY (ID),
	ADD KEY Alkat_ID_Termek (Alkat_ID),
	ADD CONSTRAINT Alkat_ID_Termek FOREIGN KEY (Alkat_ID) REFERENCES Alkategoria (ID) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Termek (ID, Nev, Kiszereles, Ar, Akcio, Leiras, Src, Alkat_ID) VALUES
	(1, 'Coca-Cola', '0,5 liter', 350, 0, 'fél literes Coca-Cola', '1.jpg', 5),
	(2, '3-húsos Pizza', '28 cm', 1900, 0, 'paradicsomos alap, sajt, sonka, darált marha és csirke hús', '2.jpg', 1),
	(3, 'Szénsavmentes ásványvíz', '0,5 liter', 250, 0, 'fél literes szénsavmentes Naturaqua ásványvíz', '3.jpg', 6),
	(4, 'Margareta Pizza', '28 cm', 1200, 1, 'paradicsomos alap, sajt', '4.jpg', 1);

	
	
CREATE TABLE Felhasznalo (
	ID int(6) NOT NULL, 
	Felhasznalonev varchar(20) NOT NULL,
	Jelszo varchar(100) NOT NULL,
	Email varchar(50) NOT NULL,
	Aktiv boolean DEFAULT 1,
	Sikertelen_login int(1) DEFAULT 0,
	Admin boolean DEFAULT 0,
	Vez_nev varchar(50) DEFAULT NULL,
	Ker_nev varchar(50) DEFAULT NULL,
	Nem varchar(10) DEFAULT NULL,
	Szulido DATE DEFAULT NULL,
	Regisztracio TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	Egyenleg int(8) DEFAULT 0,
	Fb_url varchar(50) DEFAULT NULL,
	Mod_Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	Profil_src varchar(100) DEFAULT NULL,
	Varos_ZIP int(6) DEFAULT NULL,
	Utca varchar(50) DEFAULT NULL,
	Hazszam int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
	
ALTER TABLE Felhasznalo
	ADD PRIMARY KEY (ID),
	ADD KEY Varos_ZIP_Felhasznalo (Varos_ZIP),
	ADD CONSTRAINT Varos_ZIP_Felhasznalo FOREIGN KEY (Varos_ZIP) REFERENCES Varos (ZIP) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Felhasznalo (ID, Felhasznalonev, Jelszo, Email, Admin, Vez_nev, Ker_nev, Nem, Szulido, Fb_url, Varos_ZIP, Utca, Hazszam) VALUES
	(1, 'kircsevics', 'asd123', 'kircsevics@hotmail.com', 1, 'Kiri', 'Dániel', 'F', '1994-09-19', 'fb.com', 6783, "Fő út", 1),
	(2, 'fodi.eliza0012', 'asd123', 'fodi.eliza0012@gmail.com', 0, 'Fődi', 'Eliza', 'N', '1996-04-10', 'fb.com', 6783, "Mellék út", 2); 



CREATE TABLE Rendeles (
	ID int(6) NOT NULL,
	Rend_Datum TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	Fiz_Mod varchar(30) NOT NULL,
	Teljesitve boolean DEFAULT 0,
	Mod_Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	Felh_ID int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Rendeles
	ADD PRIMARY KEY (ID),
	ADD KEY Felh_ID_Rendeles (Felh_ID),
	ADD CONSTRAINT Felh_ID_Rendeles FOREIGN KEY (Felh_ID) REFERENCES Felhasznalo (ID) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Rendeles (ID, Fiz_Mod, Felh_ID) VALUES
	(1, 'Bankkártya', 1),
	(2, 'Online Egyenleg', 2),
	(3, 'Helyszíni fizetés', 1);



CREATE TABLE Rendelesegyseg (
	ID int(6) NOT NULL,
	Mennyiseg int(6) NOT NULL,
	Termek_ID int(6) DEFAULT NULL,
	Rendeles_ID int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Rendelesegyseg
	ADD PRIMARY KEY (ID),
	ADD KEY Termek_ID_Rendelesegyseg (Termek_ID),
	ADD KEY Rendeles_ID_Rendelesegyseg (Rendeles_ID),
	ADD CONSTRAINT Termek_ID_Rendelesegyseg FOREIGN KEY (Termek_ID) REFERENCES Termek (ID) ON DELETE SET NULL ON UPDATE CASCADE,
	ADD CONSTRAINT Rendeles_ID_Rendelesegyseg FOREIGN KEY (Rendeles_ID) REFERENCES Rendeles (ID) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Rendelesegyseg (ID, Mennyiseg, Termek_ID, Rendeles_ID) VALUES
	(1, 1, 2, 1),
	(2, 1, 1, 1),
	(3, 2, 1, 2),
	(4, 3, 2, 3);



CREATE TABLE Menu (
	ID int(6) NOT NULL,
	Nev varchar(30) NOT NULL,
	Leiras varchar(255) NOT NULL,
	Letrehoz_Datum TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	Valid_From DATE DEFAULT NULL,
	Valid_To DATE DEFAULT NULL,
	Mod_Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Menu
	ADD PRIMARY KEY (ID);
	
INSERT INTO Menu (ID, Nev, Leiras, Valid_From, Valid_To) VALUES
	(1, 'Húsimádó menü', "Azoknak, akik szeretnek húst hússal ennni", '2018-10-26', '2018-12-24'),
	(2, 'Vegetáriánus menü', "A vegetáriánus vendégeknek", '2018-10-26', '2018-12-24');



CREATE TABLE Menuegyseg (
	ID int(6) NOT NULL,
	Termek_ID int(6) DEFAULT NULL,
	Menu_ID int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Menuegyseg
	ADD PRIMARY KEY (ID),
	ADD KEY Termek_ID_Menuegyseg (Termek_ID),
	ADD KEY Menu_ID_Menuegyseg (Menu_ID),
	ADD CONSTRAINT Termek_ID_Menuegyseg FOREIGN KEY (Termek_ID) REFERENCES Termek (ID) ON DELETE SET NULL ON UPDATE CASCADE,
	ADD CONSTRAINT Menu_ID_Menuegyseg FOREIGN KEY (Menu_ID) REFERENCES Menu (ID) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Menuegyseg (ID, Termek_ID, Menu_ID) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 2),
	(4, 4, 2);



CREATE TABLE Hir (
	ID int(6) NOT NULL,
	Nev varchar(30) NOT NULL,
	Leiras varchar(255) NOT NULL,
	Letrehoz_Datum TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	Felh_ID int(6) DEFAULT NULL,
	Mod_Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

ALTER TABLE Hir
	ADD PRIMARY KEY (ID),
	ADD KEY Felh_ID_Hir (Felh_ID),
	ADD CONSTRAINT Felh_ID_Hir FOREIGN KEY (Felh_ID) REFERENCES Felhasznalo (ID) ON DELETE SET NULL ON UPDATE CASCADE;
	
INSERT INTO Hir (ID, Nev, Leiras, Felh_ID) VALUES
	(1, 'Fejlesztés kezdete', 'A fejlesztést megkezdte a the5Crew csapata.', 2),
	(2, 'Várható átadás', 'Az átadás Dr. Kertész Attila számára várhatóan: 2018. November 24.', 2);
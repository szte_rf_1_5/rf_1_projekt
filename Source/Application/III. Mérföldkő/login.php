<?php
include "menu.php";

if (isset($_POST["username"])) {
		login_do();
		footer();
	} else if(isset($_SESSION["user"])){
        ?><script language="JavaScript">
			document.location.href ="index.php";
		</script><?php
	} else {
        login_data("");
        footer();
    }

function login_data($error) {
	echo '<h2>Belépés</h2>';
	?>
	<form align="center" method="post" action="login.php">
		<table>
		<tr>
		<th>Felhasználónév:</th>
		<td><input type="text" name="username"/></td>
		</tr>
		<tr>
		<th>Jelszó:</th>
		<td><input type="password" name="password"/></td>
		</tr>
		</table>
		</br>
		<input type="submit" value="Belépés"/>
	</form>
	<span align="center" style="color:red">
		<?php
			echo "</br>";
			echo $error;
			echo "</br>";
			echo "</br>";
		?>
	</span>
	<strong align="center">Nincs még fiókod?</strong>
	<form align="center" method="post" action="regist.php">
		<input type="submit" value="Regisztráció"/>
	</form>
	<?php
}

function login_do(){
	
	if (($_POST["username"]) == "" || ($_POST["password"]) == "") {
		login_data("Hiányzó adatok!");
	}else{
		if ( !($conn = connect() )) { 
        	return false;
    	}

		$Username = ($_POST["username"]);
        //$Sha_password = sha1($_POST["password"]);
        $Sha_password = ($_POST["password"]);
		
		$row = get_user($Username);
		
        if (user_test($Username)) {
			if ($row["JELSZO"] == $Sha_password) {
				if($row["AKTIV"]==1){
					if ($row["ADMIN"] == 1) {
					$_SESSION["admin"] = true;
					} else {
					$_SESSION["admin"] = false;
					}
					$_SESSION["user"] = $_POST["username"];
					$_SESSION["user_id"] = $row["ID"];
					reset_attempt($Username);
					?>
					<script language="JavaScript">
						document.location.href = "index.php";
					</script><?php
				} else {
					login_data("Blokkolt felhasználói fiók. Kérlek vedd fel a kapcsolatot az oldal adminisztrátoraival!");
				}
            } else {
				if($row["AKTIV"]==1){
					if($row["SIKERTELEN_LOGIN"]+1==3){
						block_user($Username);
						login_data("A fiókod blokkolásra került a 3 sikertelen bejelentkezést követően! Kérlek vedd fel a kapcsolatot az oldal adminisztrátoraival!");
					} else {
						add_failed_attempt($Username);
						login_data("Rossz jelszó!");
					}
				}else {
					login_data("Rossz jelszó!");
				}
			}
		}else {
				login_data("Nincs ilyen felhasználó!");
				mysqli_close($conn);
		}
	}
}



function user_test($Username) {
		if ( !($conn = connect() )) { 
        return false;
    	}

		$sql = ("SELECT count(FELHASZNALONEV) AS num FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $Username . "'");
		$result = mysqli_query( $conn, $sql );
		$row = mysqli_fetch_assoc($result);
		
		if ($row["num"] == 0) {
			mysqli_close($conn);
			return false;
		} else {
			mysqli_close($conn);
			return true;
		}
}

function get_user($Username){
	if ( !($conn = connect() )) { 
        return false;
    	}
	$sql= ("SELECT ID, JELSZO, ADMIN, AKTIV, SIKERTELEN_LOGIN FROM FELHASZNALO WHERE FELHASZNALONEV = '" . $Username . "'");	
	$result = mysqli_query( $conn, $sql );	
	$row = mysqli_fetch_assoc($result);
	return $row;
}

function block_user($Username){
	if ( !($conn = connect() )) { 
        return false;
    	}
	$stmt = mysqli_prepare( $conn, "UPDATE FELHASZNALO SET SIKERTELEN_LOGIN = 0, AKTIV=0 WHERE FELHASZNALONEV='" . $Username . "'");
	$sikeres=mysqli_stmt_execute($stmt);
}

function add_failed_attempt($Username){
	if ( !($conn = connect() )) { 
        return false;
    	}
	$stmt = mysqli_prepare( $conn, "UPDATE FELHASZNALO SET SIKERTELEN_LOGIN = SIKERTELEN_LOGIN + 1 WHERE FELHASZNALONEV='" . $Username . "' ");
	$sikeres=mysqli_stmt_execute($stmt);
}	

function reset_attempt($Username){
	if ( !($conn = connect() )) { 
        return false;
    	}
	$stmt = mysqli_prepare( $conn, "UPDATE FELHASZNALO SET SIKERTELEN_LOGIN = 0 WHERE FELHASZNALONEV='" . $Username . "' ");
	$sikeres=mysqli_stmt_execute($stmt);
}	

?>
<!DOCTYPE html>
<html>
<head>
    <title>eMeal Online Étterem</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div class="w3-top">
    <div class="w3-bar w3-white w3-wide w3-padding w3-card">
        <a href="index.php" class="w3-bar-item w3-button"><b>eMeal</b> Online Étterem</a>
        <div class="w3-right w3-hide-small">
			<?php
            include "fgv.php";
			session_start();
			if (isset($_SESSION['user'])) {
                ?>
                <a href="rendelesek.php" class="w3-bar-item w3-button">Rendeléseim</a>
				<?php
			}
			?>
            <a href="termekek.php" class="w3-bar-item w3-button">Termékek</a>
            <a href="kereses.php" class="w3-bar-item w3-button">Keresés</a>
            <a href="menus.php" class="w3-bar-item w3-button">Menük</a>
            <a href="akciok.php" class="w3-bar-item w3-button">Akciók</a>
			<a href="elerhetosegek.php" class="w3-bar-item w3-button">Elérhetőségek</a>
            <?php
            if(!isset($_SESSION["user"])){
                ?><a href="login.php" class="w3-bar-item w3-button">Belépés / Regisztráció</a><?php
            } else{
                echo '<a href="felhasznalo_adatok.php" class="w3-bar-item w3-button" style="background: gainsboro" id="felhasznalo">Belépve: '.$_SESSION["user"].'</a>';
                echo '<a id="belepes" href="logout.php"
               class="w3-bar-item w3-button">Kilépés</a>';
            }
			?>
        </div>
		<div class="w3-right w3-hide-small">
			<?php
			if (isset($_SESSION["user"])) {
                if (admin_e($_SESSION["user"])) {
                    ?>
					<a class="w3-bar-item w3-button" text><b>ADMIN MENÜ</b></a>
                    <a href="rendeles_kezeles.php" class="w3-bar-item w3-button">Rendelések</a>
                    <a href="felhasznalo_kezeles.php" class="w3-bar-item w3-button">Felhasználók</a>
					<a href="termek_kezeles.php" class="w3-bar-item w3-button">Termékek</a>
                    <a href="hir_kezeles.php" class="w3-bar-item w3-button">Hírek</a>
                    <a href="menu_kezeles.php" class="w3-bar-item w3-button">Menük</a>
					<?php
				}
			}
			?>
		</div>
    </div>
</div>
<br/>
<br/>
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
    <img class="w3-image" src="images/header.png" alt="Header" width="1500" height="800">
    <div class="w3-display-middle w3-margin-top w3-center">
        <h1 class="w3-xxlarge w3-text-white"><span class="w3-padding w3-black w3-opacity-min"><b>eMeal</b></span> <span class="w3-hide-small w3-text-light-grey">Online Étterem</span></h1>
    </div>
</header>
<div class="w3-content w3-padding" style="max-width:1564px">

</div>
</body>
</html>




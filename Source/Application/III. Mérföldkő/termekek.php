<?php

include "menu.php";

if (isset($_POST["add_cart"])) {
    kosarba();
    get_termekek();
}else{
	get_termekek();
	footer();
}


function get_termekek(){
	?>
	<h2>Termékek</h2>
	</br>
    <h3 id="title">Ételek</h3>
	    	<?php
	    		get_pizzak();
	    		get_gyrosok();
	    		get_hambik();
	    		get_koretek();
	    	?>
	    </br>
    <h3 id="title">Italok</h3>
	    	<?php
	    		get_szensavas();
	    		get_mentes();
	    	?>
	<?php
}

function count_test($alkat){
	if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(ID) AS num FROM TERMEK WHERE ALKAT_ID = (SELECT ID FROM ALKATEGORIA WHERE NEV='" . $alkat . "')");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    if ($row["num"] == 0) {
        mysqli_close($conn);
        return false;
    } else {
        mysqli_close($conn);
        return true;
    }
}

function count_alkat($alkat){
	if ( !($conn = connect() )) { 
        return false;
    }

    $sql = ("SELECT count(ID) AS num FROM TERMEK WHERE ALKAT_ID = (SELECT ID FROM ALKATEGORIA WHERE NEV='" . $alkat . "')");
    $result = mysqli_query( $conn, $sql );
    $row = mysqli_fetch_assoc($result);
    
    mysqli_close($conn);
    
    return $row["num"];
}


function pretable(){
	?>
     <table style="width: 60%">
     <thead style="font-weight: bold">
 				<tr>
 					<td style="width: 9%;text-align: left;">Kép</td>
 					<td style="width: 12%;text-align: left;">Név</td>
 					<td style="width: 8%;text-align: left;">Kiszerelés</td>
 					<td style="width: 41%;text-align: left;">Leírás</td>
 					<td style="width: 4%;text-align: left;">Ár</td>
 					<td style="width: 5%;text-align: left;">Akció</td>
 					<td style="width: 5%;text-align: left;">Kosárba</td>
 				</tr>
 	</thead>
 	</table>
 	<?php
}

function detail_table($row){
	?>
	<table style="width: 60%">
		<tr>
			<td style="width: 7%;text-align: left;">
			<?php
				if(!is_null($row["SRC"])){
					?>
					<img src="images/<?php echo $row["SRC"]?>" style="float:left;width: 100px; height: 100px;">
					<?php
				}else{
					?>
					<img src="images/soon.png" style="float:left;width: 100px; height: 100px;">
					<?php
				}
			?>
			</td>
			<td style="width: 14%;text-align: justify;"><?php echo $row["NEV"]?></td>
			<td style="width: 7%;text-align: left;"><?php echo $row["KISZERELES"]?></td>
			<td style="text-align: justify;width: 40%;"><?php echo $row["LEIRAS"]?></td>
			<td style="width: 7%;text-align: left;"><?php echo $row["AR"]." Ft"?></td>
			<td style="width: 6%;text-align: left;">
				<?php
				if($row["AKCIO"]){
					?>
						<img src="images/discount.png" style="width: 30px; height: 30px;" title="25% kedvezmény">
					<?php
				}
				?>	
			</td>
			<td style="width: 5%;">
				<form method="post" action="termekek.php">
					<input src="images/cart.png" style="width: 30px; height: 30px;" type="image">
					<input type="hidden" name="add_cart" value="<?php echo $row["ID"]?>">
				</form>
			</td>
		</tr>
	</table>
	<?php
}

function get_pizzak(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Pizza")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Pizza')");
	    $result = mysqli_query( $conn, $sql );

		?>
		<hr id="kisvonal" />
		   <p>Pizzák (<?php echo count_alkat("Pizza");?>)</p>  
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_gyrosok(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Gyros")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Gyros')");
	    $result = mysqli_query( $conn, $sql );
		 
		?>
		<hr id="kisvonal" />
		   <p>Gyrosok (<?php echo count_alkat("Gyros");?>)</p>
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_hambik(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Hamburger")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Hamburger')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Hamburgerek (<?php echo count_alkat("Hamburger");?>)</p>
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_koretek(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Köret")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Köret')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Köretek (<?php echo count_alkat("Köret");?>)</p>
		<?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}

}

function get_szensavas(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Szénsavas")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Szénsavas')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Szénsavas (<?php echo count_alkat("Szénsavas");?>)</p>
		 <?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

function get_mentes(){
	if ( !($conn = connect() )) { 
        	return false;
    }
    if(count_test("Szénsavmentes")){
	    $sql = ("SELECT ID, NEV, KISZERELES, AR, LEIRAS, SRC, AKCIO FROM TERMEK WHERE ALKAT_ID=(SELECT ID FROM ALKATEGORIA WHERE NEV='Szénsavmentes')");
	    $result = mysqli_query( $conn, $sql );
		 
		 ?>
		 <hr id="kisvonal" />
		    <p>Szénsavmentes (<?php echo count_alkat("Szénsavmentes");?>)</p>
		 <?php
		pretable();

		while($row = mysqli_fetch_assoc($result)){
			detail_table($row);
     	}
	}
}

?>